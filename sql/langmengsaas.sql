/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50522
 Source Host           : localhost:3306
 Source Schema         : langmengsaas

 Target Server Type    : MySQL
 Target Server Version : 50522
 File Encoding         : 65001

*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_businesscard
-- ----------------------------
DROP TABLE IF EXISTS `base_businesscard`;
CREATE TABLE `base_businesscard`  (
                                    `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '名片ID',
                                    `full_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
                                    `company` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司',
                                    `title` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '职务',
                                    `phone` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话',
                                    `postal_code` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮编',
                                    `mobile` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机',
                                    `address` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址',
                                    `email` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
                                    `office_phone` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '办公电话',
                                    `qq` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'QQ',
                                    `fax` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '传真',
                                    `web` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主页',
                                    `bank_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '银行',
                                    `bank_account` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账户',
                                    `tax_account` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '税号',
                                    `personal` int(11) NULL DEFAULT NULL COMMENT '是否个人',
                                    `deletionstate_code` int(11) NULL DEFAULT NULL COMMENT '删除标志',
                                    `sort_code` int(11) NULL DEFAULT NULL COMMENT '显示顺序',
                                    `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
                                    `tenant_id` bigint(20) NULL DEFAULT NULL COMMENT '租户ID',
                                    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10000011 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '名片管理' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of base_businesscard
-- ----------------------------
INSERT INTO `base_businesscard` VALUES (10000007, '刘强东', '京东', '部门经理', '037188888888', '413000', '15836985214', '河南省郑州市人民路1号', NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, 0, NULL, '9999976', 1);
INSERT INTO `base_businesscard` VALUES (10000009, '周鸿祎', '360', '售前支持部经理', '', '413000', '15874125896', '河南省郑州市建设路1号', NULL, NULL, NULL, NULL, NULL, '', '', '', 0, 0, NULL, '10000091', 1);
INSERT INTO `base_businesscard` VALUES (10000010, '测试', '678', '董事长', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, '', 2);

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
                            `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
                            `table_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表名称',
                            `table_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表描述',
                            `sub_table_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
                            `sub_table_fk_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
                            `class_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '实体类名称',
                            `tpl_category` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作 sub主子表操作）',
                            `package_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
                            `module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
                            `business_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
                            `function_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
                            `function_author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
                            `gen_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
                            `gen_path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
                            `options` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
                            `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                            `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                            `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                            `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                            `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                            PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (2, 'platform_dept', '部门表', NULL, NULL, 'PlatformDept', 'crud', 'com.ruoyi.platform', 'platform', 'dept', '部门', 'langmeng', '0', '/', NULL, 'admin', '2022-07-21 22:40:06', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (3, 'platform_logininfor', '系统访问记录', NULL, NULL, 'PlatformLogininfor', 'crud', 'com.ruoyi.platform', 'platform', 'logininfor', '系统访问记录', 'langmeng', '0', '/', NULL, 'admin', '2022-07-21 22:40:06', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (4, 'platform_menu', '菜单权限表', NULL, NULL, 'PlatformMenu', 'crud', 'com.ruoyi.platform', 'platform', 'menu', '菜单权限', 'langmeng', '0', '/', NULL, 'admin', '2022-07-21 22:40:06', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (5, 'platform_oper_log', '操作日志记录', NULL, NULL, 'PlatformOperLog', 'crud', 'com.ruoyi.platform', 'platform', 'log', '操作日志记录', 'langmeng', '0', '/', NULL, 'admin', '2022-07-21 22:40:06', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (6, 'platform_post', '岗位信息表', NULL, NULL, 'PlatformPost', 'crud', 'com.ruoyi.platform', 'platform', 'post', '岗位信息', 'langmeng', '0', '/', NULL, 'admin', '2022-07-21 22:40:06', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (7, 'platform_role', '角色信息表', NULL, NULL, 'PlatformRole', 'crud', 'com.ruoyi.platform', 'platform', 'role', '角色信息', 'langmeng', '0', '/', NULL, 'admin', '2022-07-21 22:40:06', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (8, 'platform_role_dept', '角色和部门关联表', NULL, NULL, 'PlatformRoleDept', 'crud', 'com.ruoyi.platform', 'platform', 'dept', '角色和部门关联', 'langmeng', '0', '/', NULL, 'admin', '2022-07-21 22:40:06', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (9, 'platform_role_menu', '角色和菜单关联表', NULL, NULL, 'PlatformRoleMenu', 'crud', 'com.ruoyi.platform', 'platform', 'menu', '角色和菜单关联', 'langmeng', '0', '/', NULL, 'admin', '2022-07-21 22:40:06', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (11, 'platform_user', '用户信息表', NULL, NULL, 'PlatformUser', 'crud', 'com.ruoyi.platform', 'platform', 'user', '用户信息', 'langmeng', '0', '/', NULL, 'admin', '2022-07-21 22:40:06', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (12, 'platform_user_post', '用户与岗位关联表', NULL, NULL, 'PlatformUserPost', 'crud', 'com.ruoyi.platform', 'platform', 'post', '用户与岗位关联', 'langmeng', '0', '/', NULL, 'admin', '2022-07-21 22:40:06', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (13, 'platform_user_role', '用户和角色关联表', NULL, NULL, 'PlatformUserRole', 'crud', 'com.ruoyi.platform', 'platform', 'role', '用户和角色关联', 'langmeng', '0', '/', NULL, 'admin', '2022-07-21 22:40:06', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (14, 'platform_tenant', '租户表', '', NULL, 'PlatformTenant', 'crud', 'com.langmeng.platform', 'platform', 'tenant', '租户管理', 'ruoyi', '0', '/', '{\"parentMenuId\":\"1\",\"treeName\":\"\",\"treeParentCode\":\"\",\"parentMenuName\":\"系统管理\",\"treeCode\":\"\"}', 'admin', '2022-07-29 17:43:23', '', '2022-07-29 17:44:53', '');
INSERT INTO `gen_table` VALUES (16, 'base_businesscard', '名片管理', '', NULL, 'BaseBusinesscard', 'crud', 'com.langmeng.base', 'base', 'businesscard', '名片管理', '大猫咪', '0', '/', '{\"parentMenuId\":\"\",\"treeName\":\"\",\"treeParentCode\":\"\",\"parentMenuName\":\"\",\"treeCode\":\"\"}', 'admin', '2022-08-02 20:06:12', '', '2022-08-02 20:07:50', '');

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
                                   `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
                                   `table_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '归属表编号',
                                   `column_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列名称',
                                   `column_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列描述',
                                   `column_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列类型',
                                   `java_type` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
                                   `java_field` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
                                   `is_pk` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
                                   `is_increment` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
                                   `is_required` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
                                   `is_insert` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
                                   `is_edit` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
                                   `is_list` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
                                   `is_query` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
                                   `query_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
                                   `html_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
                                   `dict_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
                                   `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
                                   `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                                   `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                   `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                                   `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                   PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 188 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (15, '2', 'dept_id', '部门id', 'bigint(20)', 'Long', 'deptId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (16, '2', 'parent_id', '父部门id', 'bigint(20)', 'Long', 'parentId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (17, '2', 'ancestors', '祖级列表', 'varchar(50)', 'String', 'ancestors', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (18, '2', 'dept_name', '部门名称', 'varchar(30)', 'String', 'deptName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 4, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (19, '2', 'order_num', '显示顺序', 'int(4)', 'Integer', 'orderNum', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (20, '2', 'leader', '负责人', 'varchar(20)', 'String', 'leader', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (21, '2', 'phone', '联系电话', 'varchar(11)', 'String', 'phone', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (22, '2', 'email', '邮箱', 'varchar(50)', 'String', 'email', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (23, '2', 'status', '部门状态（0正常 1停用）', 'char(1)', 'String', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 9, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (24, '2', 'del_flag', '删除标志（0代表存在 2代表删除）', 'char(1)', 'String', 'delFlag', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 10, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (25, '2', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 11, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (26, '2', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 12, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (27, '2', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 13, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (28, '2', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 14, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (29, '3', 'info_id', '访问ID', 'bigint(20)', 'Long', 'infoId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (30, '3', 'login_name', '登录账号', 'varchar(50)', 'String', 'loginName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (31, '3', 'ipaddr', '登录IP地址', 'varchar(128)', 'String', 'ipaddr', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (32, '3', 'login_location', '登录地点', 'varchar(255)', 'String', 'loginLocation', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (33, '3', 'browser', '浏览器类型', 'varchar(50)', 'String', 'browser', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (34, '3', 'os', '操作系统', 'varchar(50)', 'String', 'os', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (35, '3', 'status', '登录状态（0成功 1失败）', 'char(1)', 'String', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 7, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (36, '3', 'msg', '提示消息', 'varchar(255)', 'String', 'msg', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (37, '3', 'login_time', '访问时间', 'datetime', 'Date', 'loginTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 9, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (38, '4', 'menu_id', '菜单ID', 'bigint(20)', 'Long', 'menuId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (39, '4', 'menu_name', '菜单名称', 'varchar(50)', 'String', 'menuName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (40, '4', 'parent_id', '父菜单ID', 'bigint(20)', 'Long', 'parentId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (41, '4', 'order_num', '显示顺序', 'int(4)', 'Integer', 'orderNum', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (42, '4', 'url', '请求地址', 'varchar(200)', 'String', 'url', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (43, '4', 'target', '打开方式（menuItem页签 menuBlank新窗口）', 'varchar(20)', 'String', 'target', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (44, '4', 'menu_type', '菜单类型（M目录 C菜单 F按钮）', 'char(1)', 'String', 'menuType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 7, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (45, '4', 'visible', '菜单状态（0显示 1隐藏）', 'char(1)', 'String', 'visible', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (46, '4', 'is_refresh', '是否刷新（0刷新 1不刷新）', 'char(1)', 'String', 'isRefresh', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (47, '4', 'perms', '权限标识', 'varchar(100)', 'String', 'perms', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (48, '4', 'icon', '菜单图标', 'varchar(100)', 'String', 'icon', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (49, '4', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 12, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (50, '4', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 13, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (51, '4', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 14, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (52, '4', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 15, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (53, '4', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 16, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (54, '5', 'oper_id', '日志主键', 'bigint(20)', 'Long', 'operId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (55, '5', 'title', '模块标题', 'varchar(50)', 'String', 'title', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (56, '5', 'business_type', '业务类型（0其它 1新增 2修改 3删除）', 'int(2)', 'Integer', 'businessType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 3, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (57, '5', 'method', '方法名称', 'varchar(100)', 'String', 'method', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (58, '5', 'request_method', '请求方式', 'varchar(10)', 'String', 'requestMethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (59, '5', 'operator_type', '操作类别（0其它 1后台用户 2手机端用户）', 'int(1)', 'Integer', 'operatorType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 6, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (60, '5', 'oper_name', '操作人员', 'varchar(50)', 'String', 'operName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 7, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (61, '5', 'dept_name', '部门名称', 'varchar(50)', 'String', 'deptName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 8, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (62, '5', 'oper_url', '请求URL', 'varchar(255)', 'String', 'operUrl', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (63, '5', 'oper_ip', '主机地址', 'varchar(128)', 'String', 'operIp', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (64, '5', 'oper_location', '操作地点', 'varchar(255)', 'String', 'operLocation', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (65, '5', 'oper_param', '请求参数', 'varchar(2000)', 'String', 'operParam', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 12, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (66, '5', 'json_result', '返回参数', 'varchar(2000)', 'String', 'jsonResult', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 13, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (67, '5', 'status', '操作状态（0正常 1异常）', 'int(1)', 'Integer', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 14, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (68, '5', 'error_msg', '错误消息', 'varchar(2000)', 'String', 'errorMsg', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 15, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (69, '5', 'oper_time', '操作时间', 'datetime', 'Date', 'operTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 16, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (70, '6', 'post_id', '岗位ID', 'bigint(20)', 'Long', 'postId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (71, '6', 'post_code', '岗位编码', 'varchar(64)', 'String', 'postCode', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (72, '6', 'post_name', '岗位名称', 'varchar(50)', 'String', 'postName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (73, '6', 'post_sort', '显示顺序', 'int(4)', 'Integer', 'postSort', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (74, '6', 'status', '状态（0正常 1停用）', 'char(1)', 'String', 'status', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'radio', '', 5, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (75, '6', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 6, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (76, '6', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 7, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (77, '6', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 8, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (78, '6', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (79, '6', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 10, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (80, '7', 'role_id', '角色ID', 'bigint(20)', 'Long', 'roleId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (81, '7', 'role_name', '角色名称', 'varchar(30)', 'String', 'roleName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (82, '7', 'role_key', '角色权限字符串', 'varchar(100)', 'String', 'roleKey', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (83, '7', 'role_sort', '显示顺序', 'int(4)', 'Integer', 'roleSort', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (84, '7', 'data_scope', '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）', 'char(1)', 'String', 'dataScope', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (85, '7', 'status', '角色状态（0正常 1停用）', 'char(1)', 'String', 'status', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'radio', '', 6, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (86, '7', 'del_flag', '删除标志（0代表存在 2代表删除）', 'char(1)', 'String', 'delFlag', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 7, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (87, '7', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 8, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (88, '7', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (89, '7', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 10, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (90, '7', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 11, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (91, '7', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 12, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (92, '8', 'role_id', '角色ID', 'bigint(20)', 'Long', 'roleId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (93, '8', 'dept_id', '部门ID', 'bigint(20)', 'Long', 'deptId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 2, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (94, '9', 'role_id', '角色ID', 'bigint(20)', 'Long', 'roleId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (95, '9', 'menu_id', '菜单ID', 'bigint(20)', 'Long', 'menuId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 2, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (108, '11', 'user_id', '用户ID', 'bigint(20)', 'Long', 'userId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (109, '11', 'dept_id', '部门ID', 'bigint(20)', 'Long', 'deptId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (110, '11', 'login_name', '登录账号', 'varchar(30)', 'String', 'loginName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (111, '11', 'user_name', '用户昵称', 'varchar(30)', 'String', 'userName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 4, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (112, '11', 'user_type', '用户类型（00系统用户 01注册用户）', 'varchar(2)', 'String', 'userType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 5, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (113, '11', 'email', '用户邮箱', 'varchar(50)', 'String', 'email', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (114, '11', 'phonenumber', '手机号码', 'varchar(11)', 'String', 'phonenumber', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (115, '11', 'sex', '用户性别（0男 1女 2未知）', 'char(1)', 'String', 'sex', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 8, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (116, '11', 'avatar', '头像路径', 'varchar(100)', 'String', 'avatar', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (117, '11', 'password', '密码', 'varchar(50)', 'String', 'password', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (118, '11', 'salt', '盐加密', 'varchar(20)', 'String', 'salt', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (119, '11', 'status', '帐号状态（0正常 1停用）', 'char(1)', 'String', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 12, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (120, '11', 'del_flag', '删除标志（0代表存在 2代表删除）', 'char(1)', 'String', 'delFlag', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 13, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (121, '11', 'login_ip', '最后登录IP', 'varchar(128)', 'String', 'loginIp', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 14, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (122, '11', 'login_date', '最后登录时间', 'datetime', 'Date', 'loginDate', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 15, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (123, '11', 'pwd_update_date', '密码最后更新时间', 'datetime', 'Date', 'pwdUpdateDate', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 16, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (124, '11', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 17, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (125, '11', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 18, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (126, '11', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 19, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (127, '11', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 20, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (128, '11', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 21, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (129, '12', 'user_id', '用户ID', 'bigint(20)', 'Long', 'userId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (130, '12', 'post_id', '岗位ID', 'bigint(20)', 'Long', 'postId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 2, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (131, '13', 'user_id', '用户ID', 'bigint(20)', 'Long', 'userId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (132, '13', 'role_id', '角色ID', 'bigint(20)', 'Long', 'roleId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 2, 'admin', '2022-07-21 22:40:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (133, '14', 'tenant_id', '租户ID', 'bigint(20)', 'Long', 'tenantId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-07-29 17:43:23', NULL, '2022-07-29 17:44:53');
INSERT INTO `gen_table_column` VALUES (134, '14', 'tenant_name', '租户名称', 'varchar(100)', 'String', 'tenantName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-07-29 17:43:23', NULL, '2022-07-29 17:44:53');
INSERT INTO `gen_table_column` VALUES (135, '14', 'tenant_no', '租户编号', 'varchar(50)', 'String', 'tenantNo', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-07-29 17:43:23', NULL, '2022-07-29 17:44:53');
INSERT INTO `gen_table_column` VALUES (136, '14', 'logo_url', '租户logo', 'varchar(200)', 'String', 'logoUrl', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-07-29 17:43:23', NULL, '2022-07-29 17:44:53');
INSERT INTO `gen_table_column` VALUES (137, '14', 'tenant_type', '租户类型', 'varchar(20)', 'String', 'tenantType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 5, 'admin', '2022-07-29 17:43:23', NULL, '2022-07-29 17:44:53');
INSERT INTO `gen_table_column` VALUES (138, '14', 'status', '租户状态（0正常 1停用）', 'char(1)', 'String', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 6, 'admin', '2022-07-29 17:43:23', NULL, '2022-07-29 17:44:53');
INSERT INTO `gen_table_column` VALUES (139, '14', 'remark', '备注', 'varchar(1500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 7, 'admin', '2022-07-29 17:43:23', NULL, '2022-07-29 17:44:53');
INSERT INTO `gen_table_column` VALUES (140, '14', 'start_date', '租户系统开始时间', 'datetime', 'Date', 'startDate', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 8, 'admin', '2022-07-29 17:43:23', NULL, '2022-07-29 17:44:53');
INSERT INTO `gen_table_column` VALUES (141, '14', 'end_date', '租户系统到期时间', 'datetime', 'Date', 'endDate', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 9, 'admin', '2022-07-29 17:43:23', NULL, '2022-07-29 17:44:53');
INSERT INTO `gen_table_column` VALUES (142, '14', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 10, 'admin', '2022-07-29 17:43:23', NULL, '2022-07-29 17:44:53');
INSERT INTO `gen_table_column` VALUES (143, '14', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 11, 'admin', '2022-07-29 17:43:23', NULL, '2022-07-29 17:44:53');
INSERT INTO `gen_table_column` VALUES (144, '14', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 12, 'admin', '2022-07-29 17:43:23', NULL, '2022-07-29 17:44:53');
INSERT INTO `gen_table_column` VALUES (145, '14', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 13, 'admin', '2022-07-29 17:43:23', NULL, '2022-07-29 17:44:53');
INSERT INTO `gen_table_column` VALUES (167, '16', 'id', '名片ID', 'bigint(20)', 'Long', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-08-02 20:06:12', NULL, '2022-08-02 20:07:50');
INSERT INTO `gen_table_column` VALUES (168, '16', 'full_name', '姓名', 'varchar(40)', 'String', 'fullName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-08-02 20:06:12', NULL, '2022-08-02 20:07:50');
INSERT INTO `gen_table_column` VALUES (169, '16', 'company', '公司', 'varchar(200)', 'String', 'company', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-08-02 20:06:12', NULL, '2022-08-02 20:07:50');
INSERT INTO `gen_table_column` VALUES (170, '16', 'title', '职务', 'varchar(40)', 'String', 'title', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-08-02 20:06:12', NULL, '2022-08-02 20:07:50');
INSERT INTO `gen_table_column` VALUES (171, '16', 'phone', '电话', 'varchar(40)', 'String', 'phone', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-08-02 20:06:12', NULL, '2022-08-02 20:07:50');
INSERT INTO `gen_table_column` VALUES (172, '16', 'postal_code', '邮编', 'varchar(40)', 'String', 'postalCode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-08-02 20:06:12', NULL, '2022-08-02 20:07:50');
INSERT INTO `gen_table_column` VALUES (173, '16', 'mobile', '手机', 'varchar(40)', 'String', 'mobile', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-08-02 20:06:12', NULL, '2022-08-02 20:07:50');
INSERT INTO `gen_table_column` VALUES (174, '16', 'address', '地址', 'varchar(40)', 'String', 'address', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-08-02 20:06:12', NULL, '2022-08-02 20:07:50');
INSERT INTO `gen_table_column` VALUES (175, '16', 'email', '邮箱', 'varchar(40)', 'String', 'email', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2022-08-02 20:06:12', NULL, '2022-08-02 20:07:50');
INSERT INTO `gen_table_column` VALUES (176, '16', 'office_phone', '办公电话', 'varchar(40)', 'String', 'officePhone', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2022-08-02 20:06:12', NULL, '2022-08-02 20:07:50');
INSERT INTO `gen_table_column` VALUES (177, '16', 'qq', 'QQ', 'varchar(40)', 'String', 'qq', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2022-08-02 20:06:12', NULL, '2022-08-02 20:07:50');
INSERT INTO `gen_table_column` VALUES (178, '16', 'fax', '传真', 'varchar(40)', 'String', 'fax', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 12, 'admin', '2022-08-02 20:06:12', NULL, '2022-08-02 20:07:50');
INSERT INTO `gen_table_column` VALUES (179, '16', 'web', '主页', 'varchar(40)', 'String', 'web', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 13, 'admin', '2022-08-02 20:06:12', NULL, '2022-08-02 20:07:50');
INSERT INTO `gen_table_column` VALUES (180, '16', 'bank_name', '银行', 'varchar(40)', 'String', 'bankName', '0', '0', NULL, '1', '1', NULL, NULL, 'LIKE', 'input', '', 14, 'admin', '2022-08-02 20:06:12', NULL, '2022-08-02 20:07:50');
INSERT INTO `gen_table_column` VALUES (181, '16', 'bank_account', '账户', 'varchar(40)', 'String', 'bankAccount', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 15, 'admin', '2022-08-02 20:06:12', NULL, '2022-08-02 20:07:50');
INSERT INTO `gen_table_column` VALUES (182, '16', 'tax_account', '税号', 'varchar(40)', 'String', 'taxAccount', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 16, 'admin', '2022-08-02 20:06:12', NULL, '2022-08-02 20:07:50');
INSERT INTO `gen_table_column` VALUES (183, '16', 'personal', '是否个人', 'int(11)', 'Long', 'personal', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 17, 'admin', '2022-08-02 20:06:12', NULL, '2022-08-02 20:07:50');
INSERT INTO `gen_table_column` VALUES (184, '16', 'deletionstate_code', '删除标志', 'int(11)', 'Long', 'deletionstateCode', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 18, 'admin', '2022-08-02 20:06:12', NULL, '2022-08-02 20:07:50');
INSERT INTO `gen_table_column` VALUES (185, '16', 'sort_code', '显示顺序', 'int(11)', 'Long', 'sortCode', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 19, 'admin', '2022-08-02 20:06:12', NULL, '2022-08-02 20:07:50');
INSERT INTO `gen_table_column` VALUES (186, '16', 'remark', '备注', 'text', 'String', 'remark', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'textarea', '', 20, 'admin', '2022-08-02 20:06:12', NULL, '2022-08-02 20:07:50');
INSERT INTO `gen_table_column` VALUES (187, '16', 'tenant_id', '租户ID', 'bigint(20)', 'Long', 'tenantId', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 21, 'admin', '2022-08-02 20:06:12', NULL, '2022-08-02 20:07:50');

-- ----------------------------
-- Table structure for platform_config
-- ----------------------------
DROP TABLE IF EXISTS `platform_config`;
CREATE TABLE `platform_config`  (
                                  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
                                  `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数名称',
                                  `config_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键名',
                                  `config_value` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键值',
                                  `config_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
                                  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                                  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                                  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参数配置表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of platform_config
-- ----------------------------
INSERT INTO `platform_config` VALUES (1, '主框架页-默认皮肤样式名称', 'platform.index.skinName', ' skin-purple', 'Y', 'admin', '2022-07-21 20:17:34', 'admin', '2022-08-01 10:34:48', '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `platform_config` VALUES (2, '用户管理-账号初始密码', 'platform.user.initPassword', '123456', 'Y', 'admin', '2022-07-21 20:17:34', NULL, NULL, '初始化密码 123456');
INSERT INTO `platform_config` VALUES (3, '主框架页-侧边栏主题', 'platform.index.sideTheme', 'theme-dark', 'Y', 'admin', '2022-07-21 20:17:34', NULL, NULL, '深黑主题theme-dark，浅色主题theme-light，深蓝主题theme-blue');
INSERT INTO `platform_config` VALUES (4, '账号自助-是否开启用户注册功能', 'platform.account.registerUser', 'true', 'Y', 'admin', '2022-07-21 20:17:34', NULL, NULL, '是否开启注册用户功能（true开启，false关闭）');
INSERT INTO `platform_config` VALUES (5, '用户管理-密码字符范围', 'platform.account.chrtype', '0', 'Y', 'admin', '2022-07-21 20:17:34', NULL, NULL, '默认任意字符范围，0任意（密码可以输入任意字符），1数字（密码只能为0-9数字），2英文字母（密码只能为a-z和A-Z字母），3字母和数字（密码必须包含字母，数字）,4字母数字和特殊字符（目前支持的特殊字符包括：~!@#$%^&*()-=_+）');
INSERT INTO `platform_config` VALUES (6, '用户管理-初始密码修改策略', 'platform.account.initPasswordModify', '0', 'Y', 'admin', '2022-07-21 20:17:34', NULL, NULL, '0：初始密码修改策略关闭，没有任何提示，1：提醒用户，如果未修改初始密码，则在登录时就会提醒修改密码对话框');
INSERT INTO `platform_config` VALUES (7, '用户管理-账号密码更新周期', 'platform.account.passwordValidateDays', '0', 'Y', 'admin', '2022-07-21 20:17:34', NULL, NULL, '密码更新周期（填写数字，数据初始化值为0不限制，若修改必须为大于0小于365的正整数），如果超过这个周期登录系统时，则在登录时就会提醒修改密码对话框');
INSERT INTO `platform_config` VALUES (8, '主框架页-菜单导航显示风格', 'platform.index.menuStyle', 'default', 'Y', 'admin', '2022-07-21 20:17:34', NULL, NULL, '菜单导航显示风格（default为左侧导航菜单，topnav为顶部导航菜单）');
INSERT INTO `platform_config` VALUES (9, '主框架页-是否开启页脚', 'platform.index.footer', 'true', 'Y', 'admin', '2022-07-21 20:17:34', NULL, NULL, '是否开启底部页脚显示（true显示，false隐藏）');
INSERT INTO `platform_config` VALUES (10, '主框架页-是否开启页签', 'platform.index.tagsView', 'true', 'Y', 'admin', '2022-07-21 20:17:34', NULL, NULL, '是否开启菜单多页签显示（true显示，false隐藏）');

-- ----------------------------
-- Table structure for platform_dept
-- ----------------------------
DROP TABLE IF EXISTS `platform_dept`;
CREATE TABLE `platform_dept`  (
                                `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
                                `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
                                `ancestors` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '祖级列表',
                                `dept_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
                                `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
                                `leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '负责人',
                                `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
                                `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
                                `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
                                `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
                                `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                                `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                                `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 110 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of platform_dept
-- ----------------------------
INSERT INTO `platform_dept` VALUES (100, 0, '0', '运维科技', 0, '运维', '15888888888', 'yunwei@qq.com', '0', '0', 'admin', '2022-07-21 20:17:34', '', NULL);
INSERT INTO `platform_dept` VALUES (101, 100, '0,100', '郑州总公司', 1, '运维', '15888888888', 'yunwei@qq.com', '0', '0', 'admin', '2022-07-21 20:17:34', '', NULL);
INSERT INTO `platform_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '运维', '15888888888', 'yunwei@qq.com', '0', '0', 'admin', '2022-07-21 20:17:34', '', NULL);
INSERT INTO `platform_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '运维', '15888888888', 'yunwei@qq.com', '0', '0', 'admin', '2022-07-21 20:17:34', '', NULL);
INSERT INTO `platform_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '运维', '15888888888', 'yunwei@qq.com', '0', '0', 'admin', '2022-07-21 20:17:34', '', NULL);
INSERT INTO `platform_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '运维', '15888888888', 'yunwei@qq.com', '0', '0', 'admin', '2022-07-21 20:17:34', '', NULL);
INSERT INTO `platform_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '运维', '15888888888', 'yunwei@qq.com', '0', '0', 'admin', '2022-07-21 20:17:34', '', NULL);
INSERT INTO `platform_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '运维', '15888888888', 'yunwei@qq.com', '0', '0', 'admin', '2022-07-21 20:17:34', '', NULL);
INSERT INTO `platform_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '运维', '15888888888', 'yunwei@qq.com', '0', '0', 'admin', '2022-07-21 20:17:34', '', NULL);
INSERT INTO `platform_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '运维', '15888888888', 'yunwei@qq.com', '0', '0', 'admin', '2022-07-21 20:17:34', '', NULL);

-- ----------------------------
-- Table structure for platform_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `platform_dict_data`;
CREATE TABLE `platform_dict_data`  (
                                     `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
                                     `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
                                     `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典标签',
                                     `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典键值',
                                     `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
                                     `css_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
                                     `list_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
                                     `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
                                     `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
                                     `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                                     `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                     `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                                     `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                     `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                     PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of platform_dict_data
-- ----------------------------
INSERT INTO `platform_dict_data` VALUES (1, 1, '男', '0', 'platform_user_sex', '', '', 'Y', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '性别男');
INSERT INTO `platform_dict_data` VALUES (2, 2, '女', '1', 'platform_user_sex', '', '', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '性别女');
INSERT INTO `platform_dict_data` VALUES (3, 3, '未知', '2', 'platform_user_sex', '', '', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '性别未知');
INSERT INTO `platform_dict_data` VALUES (4, 1, '显示', '0', 'platform_show_hide', '', 'primary', 'Y', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '显示菜单');
INSERT INTO `platform_dict_data` VALUES (5, 2, '隐藏', '1', 'platform_show_hide', '', 'danger', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '隐藏菜单');
INSERT INTO `platform_dict_data` VALUES (6, 1, '正常', '0', 'platform_normal_disable', '', 'primary', 'Y', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '正常状态');
INSERT INTO `platform_dict_data` VALUES (7, 2, '停用', '1', 'platform_normal_disable', '', 'danger', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '停用状态');
INSERT INTO `platform_dict_data` VALUES (8, 1, '正常', '0', 'platform_job_status', '', 'primary', 'Y', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '正常状态');
INSERT INTO `platform_dict_data` VALUES (9, 2, '暂停', '1', 'platform_job_status', '', 'danger', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '停用状态');
INSERT INTO `platform_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'platform_job_group', '', '', 'Y', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '默认分组');
INSERT INTO `platform_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'platform_job_group', '', '', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '系统分组');
INSERT INTO `platform_dict_data` VALUES (12, 1, '是', 'Y', 'platform_yes_no', '', 'primary', 'Y', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '系统默认是');
INSERT INTO `platform_dict_data` VALUES (13, 2, '否', 'N', 'platform_yes_no', '', 'danger', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '系统默认否');
INSERT INTO `platform_dict_data` VALUES (14, 1, '通知', '1', 'platform_notice_type', '', 'warning', 'Y', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '通知');
INSERT INTO `platform_dict_data` VALUES (15, 2, '公告', '2', 'platform_notice_type', '', 'success', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '公告');
INSERT INTO `platform_dict_data` VALUES (16, 1, '正常', '0', 'platform_notice_status', '', 'primary', 'Y', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '正常状态');
INSERT INTO `platform_dict_data` VALUES (17, 2, '关闭', '1', 'platform_notice_status', '', 'danger', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '关闭状态');
INSERT INTO `platform_dict_data` VALUES (18, 99, '其他', '0', 'platform_oper_type', '', 'info', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '其他操作');
INSERT INTO `platform_dict_data` VALUES (19, 1, '新增', '1', 'platform_oper_type', '', 'info', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '新增操作');
INSERT INTO `platform_dict_data` VALUES (20, 2, '修改', '2', 'platform_oper_type', '', 'info', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '修改操作');
INSERT INTO `platform_dict_data` VALUES (21, 3, '删除', '3', 'platform_oper_type', '', 'danger', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '删除操作');
INSERT INTO `platform_dict_data` VALUES (22, 4, '授权', '4', 'platform_oper_type', '', 'primary', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '授权操作');
INSERT INTO `platform_dict_data` VALUES (23, 5, '导出', '5', 'platform_oper_type', '', 'warning', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '导出操作');
INSERT INTO `platform_dict_data` VALUES (24, 6, '导入', '6', 'platform_oper_type', '', 'warning', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '导入操作');
INSERT INTO `platform_dict_data` VALUES (25, 7, '强退', '7', 'platform_oper_type', '', 'danger', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '强退操作');
INSERT INTO `platform_dict_data` VALUES (26, 8, '生成代码', '8', 'platform_oper_type', '', 'warning', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '生成操作');
INSERT INTO `platform_dict_data` VALUES (27, 9, '清空数据', '9', 'platform_oper_type', '', 'danger', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '清空操作');
INSERT INTO `platform_dict_data` VALUES (28, 1, '成功', '0', 'platform_common_status', '', 'primary', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '正常状态');
INSERT INTO `platform_dict_data` VALUES (29, 2, '失败', '1', 'platform_common_status', '', 'danger', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '停用状态');

-- ----------------------------
-- Table structure for platform_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `platform_dict_type`;
CREATE TABLE `platform_dict_type`  (
                                     `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
                                     `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典名称',
                                     `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
                                     `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
                                     `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                                     `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                     `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                                     `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                     `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                     PRIMARY KEY (`dict_id`) USING BTREE,
                                     UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of platform_dict_type
-- ----------------------------
INSERT INTO `platform_dict_type` VALUES (1, '用户性别', 'platform_user_sex', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '用户性别列表');
INSERT INTO `platform_dict_type` VALUES (2, '菜单状态', 'platform_show_hide', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '菜单状态列表');
INSERT INTO `platform_dict_type` VALUES (3, '系统开关', 'platform_normal_disable', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '系统开关列表');
INSERT INTO `platform_dict_type` VALUES (4, '任务状态', 'platform_job_status', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '任务状态列表');
INSERT INTO `platform_dict_type` VALUES (5, '任务分组', 'platform_job_group', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '任务分组列表');
INSERT INTO `platform_dict_type` VALUES (6, '系统是否', 'platform_yes_no', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '系统是否列表');
INSERT INTO `platform_dict_type` VALUES (7, '通知类型', 'platform_notice_type', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '通知类型列表');
INSERT INTO `platform_dict_type` VALUES (8, '通知状态', 'platform_notice_status', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '通知状态列表');
INSERT INTO `platform_dict_type` VALUES (9, '操作类型', 'platform_oper_type', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '操作类型列表');
INSERT INTO `platform_dict_type` VALUES (10, '系统状态', 'platform_common_status', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '登录状态列表');

-- ----------------------------
-- Table structure for platform_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `platform_logininfor`;
CREATE TABLE `platform_logininfor`  (
                                      `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
                                      `login_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录账号',
                                      `ipaddr` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
                                      `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录地点',
                                      `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
                                      `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作系统',
                                      `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
                                      `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '提示消息',
                                      `login_time` datetime NULL DEFAULT NULL COMMENT '访问时间',
                                      PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 109 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统访问记录' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of platform_logininfor
-- ----------------------------

-- ----------------------------
-- Table structure for platform_menu
-- ----------------------------
DROP TABLE IF EXISTS `platform_menu`;
CREATE TABLE `platform_menu`  (
                                `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
                                `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
                                `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
                                `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
                                `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '请求地址',
                                `target` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '打开方式（menuItem页签 menuBlank新窗口）',
                                `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
                                `visible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
                                `is_refresh` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否刷新（0刷新 1不刷新）',
                                `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
                                `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
                                `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                                `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                                `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
                                PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1069 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of platform_menu
-- ----------------------------
INSERT INTO `platform_menu` VALUES (1, '系统管理', 0, 2, '#', 'menuItem', 'M', '0', '1', '', 'fa fa-gear', 'admin', '2022-07-21 20:17:34', 'admin', '2022-07-31 08:07:13', '系统管理目录');
INSERT INTO `platform_menu` VALUES (2, '系统监控', 0, 2, '#', '', 'M', '0', '1', '', 'fa fa-video-camera', 'admin', '2022-07-21 20:17:34', '', NULL, '系统监控目录');
INSERT INTO `platform_menu` VALUES (3, '系统工具', 0, 3, '#', '', 'M', '0', '1', '', 'fa fa-bars', 'admin', '2022-07-21 20:17:34', '', NULL, '系统工具目录');
INSERT INTO `platform_menu` VALUES (100, '用户管理', 1, 1, '/platform/user', '', 'C', '0', '1', 'platform:user:view', 'fa fa-user-o', 'admin', '2022-07-21 20:17:34', '', NULL, '用户管理菜单');
INSERT INTO `platform_menu` VALUES (101, '角色管理', 1, 2, '/platform/role', '', 'C', '0', '1', 'platform:role:view', 'fa fa-user-secret', 'admin', '2022-07-21 20:17:34', '', NULL, '角色管理菜单');
INSERT INTO `platform_menu` VALUES (102, '菜单管理', 1, 3, '/platform/menu', '', 'C', '0', '1', 'platform:menu:view', 'fa fa-th-list', 'admin', '2022-07-21 20:17:34', '', NULL, '菜单管理菜单');
INSERT INTO `platform_menu` VALUES (103, '部门管理', 1, 4, '/platform/dept', '', 'C', '0', '1', 'platform:dept:view', 'fa fa-outdent', 'admin', '2022-07-21 20:17:34', '', NULL, '部门管理菜单');
INSERT INTO `platform_menu` VALUES (104, '岗位管理', 1, 5, '/platform/post', '', 'C', '0', '1', 'platform:post:view', 'fa fa-address-card-o', 'admin', '2022-07-21 20:17:34', '', NULL, '岗位管理菜单');
INSERT INTO `platform_menu` VALUES (105, '字典管理', 1, 6, '/platform/dict', '', 'C', '0', '1', 'platform:dict:view', 'fa fa-bookmark-o', 'admin', '2022-07-21 20:17:34', '', NULL, '字典管理菜单');
INSERT INTO `platform_menu` VALUES (106, '参数设置', 1, 7, '/platform/config', '', 'C', '0', '1', 'platform:config:view', 'fa fa-sun-o', 'admin', '2022-07-21 20:17:34', '', NULL, '参数设置菜单');
INSERT INTO `platform_menu` VALUES (107, '通知公告', 1, 8, '/platform/notice', '', 'C', '0', '1', 'platform:notice:view', 'fa fa-bullhorn', 'admin', '2022-07-21 20:17:34', '', NULL, '通知公告菜单');
INSERT INTO `platform_menu` VALUES (108, '日志管理', 1, 9, '#', '', 'M', '0', '1', '', 'fa fa-pencil-square-o', 'admin', '2022-07-21 20:17:34', '', NULL, '日志管理菜单');
INSERT INTO `platform_menu` VALUES (109, '在线用户', 2, 1, '/monitor/online', '', 'C', '0', '1', 'monitor:online:view', 'fa fa-user-circle', 'admin', '2022-07-21 20:17:34', '', NULL, '在线用户菜单');
INSERT INTO `platform_menu` VALUES (110, '定时任务', 2, 2, '/monitor/job', '', 'C', '0', '1', 'monitor:job:view', 'fa fa-tasks', 'admin', '2022-07-21 20:17:34', '', NULL, '定时任务菜单');
INSERT INTO `platform_menu` VALUES (111, '数据监控', 2, 3, '/monitor/data', '', 'C', '0', '1', 'monitor:data:view', 'fa fa-bug', 'admin', '2022-07-21 20:17:34', '', NULL, '数据监控菜单');
INSERT INTO `platform_menu` VALUES (112, '服务监控', 2, 4, '/monitor/server', '', 'C', '0', '1', 'monitor:server:view', 'fa fa-server', 'admin', '2022-07-21 20:17:34', '', NULL, '服务监控菜单');
INSERT INTO `platform_menu` VALUES (113, '缓存监控', 2, 5, '/monitor/cache', '', 'C', '0', '1', 'monitor:cache:view', 'fa fa-cube', 'admin', '2022-07-21 20:17:34', '', NULL, '缓存监控菜单');
INSERT INTO `platform_menu` VALUES (114, '表单构建', 3, 1, '/tool/build', '', 'C', '0', '1', 'tool:build:view', 'fa fa-wpforms', 'admin', '2022-07-21 20:17:34', '', NULL, '表单构建菜单');
INSERT INTO `platform_menu` VALUES (115, '代码生成', 3, 2, '/tool/gen', '', 'C', '0', '1', 'tool:gen:view', 'fa fa-code', 'admin', '2022-07-21 20:17:34', '', NULL, '代码生成菜单');
INSERT INTO `platform_menu` VALUES (116, '系统接口', 3, 3, '/tool/swagger', '', 'C', '0', '1', 'tool:swagger:view', 'fa fa-gg', 'admin', '2022-07-21 20:17:34', '', NULL, '系统接口菜单');
INSERT INTO `platform_menu` VALUES (500, '操作日志', 108, 1, '/monitor/operlog', '', 'C', '0', '1', 'monitor:operlog:view', 'fa fa-address-book', 'admin', '2022-07-21 20:17:34', '', NULL, '操作日志菜单');
INSERT INTO `platform_menu` VALUES (501, '登录日志', 108, 2, '/monitor/logininfor', '', 'C', '0', '1', 'monitor:logininfor:view', 'fa fa-file-image-o', 'admin', '2022-07-21 20:17:34', '', NULL, '登录日志菜单');
INSERT INTO `platform_menu` VALUES (1000, '用户查询', 100, 1, '#', '', 'F', '0', '1', 'platform:user:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1001, '用户新增', 100, 2, '#', '', 'F', '0', '1', 'platform:user:add', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1002, '用户修改', 100, 3, '#', '', 'F', '0', '1', 'platform:user:edit', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1003, '用户删除', 100, 4, '#', '', 'F', '0', '1', 'platform:user:remove', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1004, '用户导出', 100, 5, '#', '', 'F', '0', '1', 'platform:user:export', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1005, '用户导入', 100, 6, '#', '', 'F', '0', '1', 'platform:user:import', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1006, '重置密码', 100, 7, '#', '', 'F', '0', '1', 'platform:user:resetPwd', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1007, '角色查询', 101, 1, '#', '', 'F', '0', '1', 'platform:role:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1008, '角色新增', 101, 2, '#', '', 'F', '0', '1', 'platform:role:add', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1009, '角色修改', 101, 3, '#', '', 'F', '0', '1', 'platform:role:edit', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1010, '角色删除', 101, 4, '#', '', 'F', '0', '1', 'platform:role:remove', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1011, '角色导出', 101, 5, '#', '', 'F', '0', '1', 'platform:role:export', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1012, '菜单查询', 102, 1, '#', '', 'F', '0', '1', 'platform:menu:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1013, '菜单新增', 102, 2, '#', '', 'F', '0', '1', 'platform:menu:add', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1014, '菜单修改', 102, 3, '#', '', 'F', '0', '1', 'platform:menu:edit', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1015, '菜单删除', 102, 4, '#', '', 'F', '0', '1', 'platform:menu:remove', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1016, '部门查询', 103, 1, '#', '', 'F', '0', '1', 'platform:dept:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1017, '部门新增', 103, 2, '#', '', 'F', '0', '1', 'platform:dept:add', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1018, '部门修改', 103, 3, '#', '', 'F', '0', '1', 'platform:dept:edit', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1019, '部门删除', 103, 4, '#', '', 'F', '0', '1', 'platform:dept:remove', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1020, '岗位查询', 104, 1, '#', '', 'F', '0', '1', 'platform:post:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1021, '岗位新增', 104, 2, '#', '', 'F', '0', '1', 'platform:post:add', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1022, '岗位修改', 104, 3, '#', '', 'F', '0', '1', 'platform:post:edit', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1023, '岗位删除', 104, 4, '#', '', 'F', '0', '1', 'platform:post:remove', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1024, '岗位导出', 104, 5, '#', '', 'F', '0', '1', 'platform:post:export', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1025, '字典查询', 105, 1, '#', '', 'F', '0', '1', 'platform:dict:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1026, '字典新增', 105, 2, '#', '', 'F', '0', '1', 'platform:dict:add', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1027, '字典修改', 105, 3, '#', '', 'F', '0', '1', 'platform:dict:edit', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1028, '字典删除', 105, 4, '#', '', 'F', '0', '1', 'platform:dict:remove', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1029, '字典导出', 105, 5, '#', '', 'F', '0', '1', 'platform:dict:export', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1030, '参数查询', 106, 1, '#', '', 'F', '0', '1', 'platform:config:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1031, '参数新增', 106, 2, '#', '', 'F', '0', '1', 'platform:config:add', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1032, '参数修改', 106, 3, '#', '', 'F', '0', '1', 'platform:config:edit', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1033, '参数删除', 106, 4, '#', '', 'F', '0', '1', 'platform:config:remove', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1034, '参数导出', 106, 5, '#', '', 'F', '0', '1', 'platform:config:export', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1035, '公告查询', 107, 1, '#', '', 'F', '0', '1', 'platform:notice:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1036, '公告新增', 107, 2, '#', '', 'F', '0', '1', 'platform:notice:add', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1037, '公告修改', 107, 3, '#', '', 'F', '0', '1', 'platform:notice:edit', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1038, '公告删除', 107, 4, '#', '', 'F', '0', '1', 'platform:notice:remove', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1039, '操作查询', 500, 1, '#', '', 'F', '0', '1', 'monitor:operlog:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1040, '操作删除', 500, 2, '#', '', 'F', '0', '1', 'monitor:operlog:remove', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1041, '详细信息', 500, 3, '#', '', 'F', '0', '1', 'monitor:operlog:detail', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1042, '日志导出', 500, 4, '#', '', 'F', '0', '1', 'monitor:operlog:export', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1043, '登录查询', 501, 1, '#', '', 'F', '0', '1', 'monitor:logininfor:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1044, '登录删除', 501, 2, '#', '', 'F', '0', '1', 'monitor:logininfor:remove', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1045, '日志导出', 501, 3, '#', '', 'F', '0', '1', 'monitor:logininfor:export', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1046, '账户解锁', 501, 4, '#', '', 'F', '0', '1', 'monitor:logininfor:unlock', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1047, '在线查询', 109, 1, '#', '', 'F', '0', '1', 'monitor:online:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1048, '批量强退', 109, 2, '#', '', 'F', '0', '1', 'monitor:online:batchForceLogout', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1049, '单条强退', 109, 3, '#', '', 'F', '0', '1', 'monitor:online:forceLogout', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1050, '任务查询', 110, 1, '#', '', 'F', '0', '1', 'monitor:job:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1051, '任务新增', 110, 2, '#', '', 'F', '0', '1', 'monitor:job:add', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1052, '任务修改', 110, 3, '#', '', 'F', '0', '1', 'monitor:job:edit', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1053, '任务删除', 110, 4, '#', '', 'F', '0', '1', 'monitor:job:remove', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1054, '状态修改', 110, 5, '#', '', 'F', '0', '1', 'monitor:job:changeStatus', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1055, '任务详细', 110, 6, '#', '', 'F', '0', '1', 'monitor:job:detail', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1056, '任务导出', 110, 7, '#', '', 'F', '0', '1', 'monitor:job:export', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1057, '生成查询', 115, 1, '#', '', 'F', '0', '1', 'tool:gen:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1058, '生成修改', 115, 2, '#', '', 'F', '0', '1', 'tool:gen:edit', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1059, '生成删除', 115, 3, '#', '', 'F', '0', '1', 'tool:gen:remove', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1060, '预览代码', 115, 4, '#', '', 'F', '0', '1', 'tool:gen:preview', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1061, '生成代码', 115, 5, '#', '', 'F', '0', '1', 'tool:gen:code', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1062, '租户管理', 1068, 1, '/platform/tenant', 'menuItem', 'C', '0', '1', 'platform:tenant:view', '#', 'admin', '2022-07-29 17:55:57', 'admin', '2022-07-31 08:07:38', '租户管理菜单');
INSERT INTO `platform_menu` VALUES (1063, '租户管理查询', 1062, 1, '#', '', 'F', '0', '1', 'platform:tenant:list', '#', 'admin', '2022-07-29 17:55:57', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1064, '租户管理新增', 1062, 2, '#', '', 'F', '0', '1', 'platform:tenant:add', '#', 'admin', '2022-07-29 17:55:57', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1065, '租户管理修改', 1062, 3, '#', '', 'F', '0', '1', 'platform:tenant:edit', '#', 'admin', '2022-07-29 17:55:57', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1066, '租户管理删除', 1062, 4, '#', '', 'F', '0', '1', 'platform:tenant:remove', '#', 'admin', '2022-07-29 17:55:57', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1067, '租户管理导出', 1062, 5, '#', '', 'F', '0', '1', 'platform:tenant:export', '#', 'admin', '2022-07-29 17:55:57', '', NULL, '');
INSERT INTO `platform_menu` VALUES (1068, '平台管理', 0, 1, '#', 'menuItem', 'M', '0', '1', NULL, 'fa fa-server', 'admin', '2022-07-31 08:06:59', '', NULL, '');

-- ----------------------------
-- Table structure for platform_notice
-- ----------------------------
DROP TABLE IF EXISTS `platform_notice`;
CREATE TABLE `platform_notice`  (
                                  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
                                  `notice_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告标题',
                                  `notice_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
                                  `notice_content` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公告内容',
                                  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
                                  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                                  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                                  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '通知公告表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of platform_notice
-- ----------------------------
INSERT INTO `platform_notice` VALUES (1, '温馨提醒：2018-07-01 通知新版本发布啦', '2', '新版本内容', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '管理员');
INSERT INTO `platform_notice` VALUES (2, '维护通知：2018-07-01 通知系统凌晨维护', '1', '维护内容', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '管理员');

-- ----------------------------
-- Table structure for platform_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `platform_oper_log`;
CREATE TABLE `platform_oper_log`  (
                                    `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
                                    `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '模块标题',
                                    `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
                                    `method` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '方法名称',
                                    `request_method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求方式',
                                    `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
                                    `oper_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作人员',
                                    `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
                                    `oper_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求URL',
                                    `oper_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '主机地址',
                                    `oper_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作地点',
                                    `oper_param` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求参数',
                                    `json_result` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '返回参数',
                                    `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
                                    `error_msg` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '错误消息',
                                    `oper_time` datetime NULL DEFAULT NULL COMMENT '操作时间',
                                    PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志记录' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of platform_oper_log
-- ----------------------------

-- ----------------------------
-- Table structure for platform_post
-- ----------------------------
DROP TABLE IF EXISTS `platform_post`;
CREATE TABLE `platform_post`  (
                                `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
                                `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位编码',
                                `post_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位名称',
                                `post_sort` int(4) NOT NULL COMMENT '显示顺序',
                                `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
                                `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                                `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                                `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '岗位信息表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of platform_post
-- ----------------------------
INSERT INTO `platform_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `platform_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2022-07-21 20:17:34', '', NULL, '');

-- ----------------------------
-- Table structure for platform_role
-- ----------------------------
DROP TABLE IF EXISTS `platform_role`;
CREATE TABLE `platform_role`  (
                                `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
                                `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
                                `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色权限字符串',
                                `role_sort` int(4) NOT NULL COMMENT '显示顺序',
                                `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
                                `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
                                `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
                                `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                                `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                                `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色信息表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of platform_role
-- ----------------------------
INSERT INTO `platform_role` VALUES (1, '超级管理员', 'admin', 1, '1', '0', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '超级管理员');
INSERT INTO `platform_role` VALUES (2, '普通角色', 'common', 2, '2', '0', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '普通角色');

-- ----------------------------
-- Table structure for platform_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `platform_role_dept`;
CREATE TABLE `platform_role_dept`  (
                                     `role_id` bigint(20) NOT NULL COMMENT '角色ID',
                                     `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
                                     PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of platform_role_dept
-- ----------------------------
INSERT INTO `platform_role_dept` VALUES (2, 100);
INSERT INTO `platform_role_dept` VALUES (2, 101);
INSERT INTO `platform_role_dept` VALUES (2, 105);

-- ----------------------------
-- Table structure for platform_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `platform_role_menu`;
CREATE TABLE `platform_role_menu`  (
                                     `role_id` bigint(20) NOT NULL COMMENT '角色ID',
                                     `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
                                     PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of platform_role_menu
-- ----------------------------
INSERT INTO `platform_role_menu` VALUES (2, 1);
INSERT INTO `platform_role_menu` VALUES (2, 2);
INSERT INTO `platform_role_menu` VALUES (2, 3);
INSERT INTO `platform_role_menu` VALUES (2, 4);
INSERT INTO `platform_role_menu` VALUES (2, 100);
INSERT INTO `platform_role_menu` VALUES (2, 101);
INSERT INTO `platform_role_menu` VALUES (2, 102);
INSERT INTO `platform_role_menu` VALUES (2, 103);
INSERT INTO `platform_role_menu` VALUES (2, 104);
INSERT INTO `platform_role_menu` VALUES (2, 105);
INSERT INTO `platform_role_menu` VALUES (2, 106);
INSERT INTO `platform_role_menu` VALUES (2, 107);
INSERT INTO `platform_role_menu` VALUES (2, 108);
INSERT INTO `platform_role_menu` VALUES (2, 109);
INSERT INTO `platform_role_menu` VALUES (2, 110);
INSERT INTO `platform_role_menu` VALUES (2, 111);
INSERT INTO `platform_role_menu` VALUES (2, 112);
INSERT INTO `platform_role_menu` VALUES (2, 113);
INSERT INTO `platform_role_menu` VALUES (2, 114);
INSERT INTO `platform_role_menu` VALUES (2, 115);
INSERT INTO `platform_role_menu` VALUES (2, 116);
INSERT INTO `platform_role_menu` VALUES (2, 500);
INSERT INTO `platform_role_menu` VALUES (2, 501);
INSERT INTO `platform_role_menu` VALUES (2, 1000);
INSERT INTO `platform_role_menu` VALUES (2, 1001);
INSERT INTO `platform_role_menu` VALUES (2, 1002);
INSERT INTO `platform_role_menu` VALUES (2, 1003);
INSERT INTO `platform_role_menu` VALUES (2, 1004);
INSERT INTO `platform_role_menu` VALUES (2, 1005);
INSERT INTO `platform_role_menu` VALUES (2, 1006);
INSERT INTO `platform_role_menu` VALUES (2, 1007);
INSERT INTO `platform_role_menu` VALUES (2, 1008);
INSERT INTO `platform_role_menu` VALUES (2, 1009);
INSERT INTO `platform_role_menu` VALUES (2, 1010);
INSERT INTO `platform_role_menu` VALUES (2, 1011);
INSERT INTO `platform_role_menu` VALUES (2, 1012);
INSERT INTO `platform_role_menu` VALUES (2, 1013);
INSERT INTO `platform_role_menu` VALUES (2, 1014);
INSERT INTO `platform_role_menu` VALUES (2, 1015);
INSERT INTO `platform_role_menu` VALUES (2, 1016);
INSERT INTO `platform_role_menu` VALUES (2, 1017);
INSERT INTO `platform_role_menu` VALUES (2, 1018);
INSERT INTO `platform_role_menu` VALUES (2, 1019);
INSERT INTO `platform_role_menu` VALUES (2, 1020);
INSERT INTO `platform_role_menu` VALUES (2, 1021);
INSERT INTO `platform_role_menu` VALUES (2, 1022);
INSERT INTO `platform_role_menu` VALUES (2, 1023);
INSERT INTO `platform_role_menu` VALUES (2, 1024);
INSERT INTO `platform_role_menu` VALUES (2, 1025);
INSERT INTO `platform_role_menu` VALUES (2, 1026);
INSERT INTO `platform_role_menu` VALUES (2, 1027);
INSERT INTO `platform_role_menu` VALUES (2, 1028);
INSERT INTO `platform_role_menu` VALUES (2, 1029);
INSERT INTO `platform_role_menu` VALUES (2, 1030);
INSERT INTO `platform_role_menu` VALUES (2, 1031);
INSERT INTO `platform_role_menu` VALUES (2, 1032);
INSERT INTO `platform_role_menu` VALUES (2, 1033);
INSERT INTO `platform_role_menu` VALUES (2, 1034);
INSERT INTO `platform_role_menu` VALUES (2, 1035);
INSERT INTO `platform_role_menu` VALUES (2, 1036);
INSERT INTO `platform_role_menu` VALUES (2, 1037);
INSERT INTO `platform_role_menu` VALUES (2, 1038);
INSERT INTO `platform_role_menu` VALUES (2, 1039);
INSERT INTO `platform_role_menu` VALUES (2, 1040);
INSERT INTO `platform_role_menu` VALUES (2, 1041);
INSERT INTO `platform_role_menu` VALUES (2, 1042);
INSERT INTO `platform_role_menu` VALUES (2, 1043);
INSERT INTO `platform_role_menu` VALUES (2, 1044);
INSERT INTO `platform_role_menu` VALUES (2, 1045);
INSERT INTO `platform_role_menu` VALUES (2, 1046);
INSERT INTO `platform_role_menu` VALUES (2, 1047);
INSERT INTO `platform_role_menu` VALUES (2, 1048);
INSERT INTO `platform_role_menu` VALUES (2, 1049);
INSERT INTO `platform_role_menu` VALUES (2, 1050);
INSERT INTO `platform_role_menu` VALUES (2, 1051);
INSERT INTO `platform_role_menu` VALUES (2, 1052);
INSERT INTO `platform_role_menu` VALUES (2, 1053);
INSERT INTO `platform_role_menu` VALUES (2, 1054);
INSERT INTO `platform_role_menu` VALUES (2, 1055);
INSERT INTO `platform_role_menu` VALUES (2, 1056);
INSERT INTO `platform_role_menu` VALUES (2, 1057);
INSERT INTO `platform_role_menu` VALUES (2, 1058);
INSERT INTO `platform_role_menu` VALUES (2, 1059);
INSERT INTO `platform_role_menu` VALUES (2, 1060);
INSERT INTO `platform_role_menu` VALUES (2, 1061);

-- ----------------------------
-- Table structure for platform_tenant
-- ----------------------------
DROP TABLE IF EXISTS `platform_tenant`;
CREATE TABLE `platform_tenant`  (
                                  `tenant_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '租户ID',
                                  `tenant_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '租户名称',
                                  `tenant_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '租户编号',
                                  `logo_url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户logo',
                                  `tenant_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户类型',
                                  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '租户状态（0正常 1停用）',
                                  `remark` varchar(1500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                  `start_date` datetime NULL DEFAULT NULL COMMENT '租户系统开始时间',
                                  `end_date` datetime NULL DEFAULT NULL COMMENT '租户系统到期时间',
                                  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                                  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                                  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                  PRIMARY KEY (`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '租户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of platform_tenant
-- ----------------------------
INSERT INTO `platform_tenant` VALUES (1, '腾讯', '01', '33', NULL, '0', '11222', '2021-08-04 00:00:00', '2050-03-07 00:00:00', 'admin', '2021-07-03 17:39:43', 'admin', '2022-08-02 17:41:19');
INSERT INTO `platform_tenant` VALUES (2, '百度', '02', '02', NULL, '0', '02', '2022-07-01 00:00:00', '2022-07-31 00:00:00', '', '2022-07-29 17:59:40', '', NULL);

-- ----------------------------
-- Table structure for platform_user
-- ----------------------------
DROP TABLE IF EXISTS `platform_user`;
CREATE TABLE `platform_user`  (
                                `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
                                `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
                                `login_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录账号',
                                `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户昵称',
                                `user_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户 01注册用户）',
                                `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
                                `phonenumber` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '手机号码',
                                `sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
                                `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '头像路径',
                                `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '密码',
                                `salt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '盐加密',
                                `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
                                `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
                                `login_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
                                `login_date` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
                                `pwd_update_date` datetime NULL DEFAULT NULL COMMENT '密码最后更新时间',
                                `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                                `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                                `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of platform_user
-- ----------------------------
INSERT INTO `platform_user` VALUES (1, 103, 'admin', '超级管理员', '00', 'admin@163.com', '15888888888', '1', '', '29c67a30398638269fe600f73a054934', '111111', '0', '0', '127.0.0.1', '2022-08-02 23:02:00', '2022-07-21 20:17:34', 'admin', '2022-07-21 20:17:34', '', '2022-08-02 23:02:00', '管理员');
INSERT INTO `platform_user` VALUES (2, 105, 'yunwei', '运维管理员', '00', 'yunwei@qq.com', '15666666666', '1', '', '8e6d98b90472783cc73c17047ddccf36', '222222', '0', '0', '127.0.0.1', '2022-07-21 20:17:34', '2022-07-21 20:17:34', 'admin', '2022-07-21 20:17:34', '', NULL, '测试员');

-- ----------------------------
-- Table structure for platform_user_online
-- ----------------------------
DROP TABLE IF EXISTS `platform_user_online`;
CREATE TABLE `platform_user_online`  (
                                       `sessionId` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户会话id',
                                       `login_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录账号',
                                       `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
                                       `ipaddr` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
                                       `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录地点',
                                       `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
                                       `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作系统',
                                       `status` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '在线状态on_line在线off_line离线',
                                       `start_timestamp` datetime NULL DEFAULT NULL COMMENT 'session创建时间',
                                       `last_access_time` datetime NULL DEFAULT NULL COMMENT 'session最后访问时间',
                                       `expire_time` int(5) NULL DEFAULT 0 COMMENT '超时时间，单位为分钟',
                                       PRIMARY KEY (`sessionId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '在线用户记录' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of platform_user_online
-- ----------------------------

-- ----------------------------
-- Table structure for platform_user_post
-- ----------------------------
DROP TABLE IF EXISTS `platform_user_post`;
CREATE TABLE `platform_user_post`  (
                                     `user_id` bigint(20) NOT NULL COMMENT '用户ID',
                                     `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
                                     PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of platform_user_post
-- ----------------------------
INSERT INTO `platform_user_post` VALUES (1, 1);
INSERT INTO `platform_user_post` VALUES (2, 2);

-- ----------------------------
-- Table structure for platform_user_role
-- ----------------------------
DROP TABLE IF EXISTS `platform_user_role`;
CREATE TABLE `platform_user_role`  (
                                     `user_id` bigint(20) NOT NULL COMMENT '用户ID',
                                     `role_id` bigint(20) NOT NULL COMMENT '角色ID',
                                     PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of platform_user_role
-- ----------------------------
INSERT INTO `platform_user_role` VALUES (1, 1);
INSERT INTO `platform_user_role` VALUES (2, 2);

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
                                     `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
                                     `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
                                     `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
                                     `blob_data` blob NULL COMMENT '存放持久化Trigger对象',
                                     PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
                                     CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Blob类型的触发器表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
                                 `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
                                 `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '日历名称',
                                 `calendar` blob NOT NULL COMMENT '存放持久化calendar对象',
                                 PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '日历信息表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
                                     `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
                                     `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
                                     `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
                                     `cron_expression` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'cron表达式',
                                     `time_zone_id` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '时区',
                                     PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
                                     CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Cron类型的触发器表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
                                      `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
                                      `entry_id` varchar(95) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度器实例id',
                                      `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
                                      `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
                                      `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度器实例名',
                                      `fired_time` bigint(13) NOT NULL COMMENT '触发的时间',
                                      `sched_time` bigint(13) NOT NULL COMMENT '定时器制定的时间',
                                      `priority` int(11) NOT NULL COMMENT '优先级',
                                      `state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态',
                                      `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务名称',
                                      `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务组名',
                                      `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否并发',
                                      `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否接受恢复执行',
                                      PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '已触发的触发器表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
                                   `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
                                   `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务名称',
                                   `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务组名',
                                   `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '相关介绍',
                                   `job_class_name` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '执行任务类名称',
                                   `is_durable` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否持久化',
                                   `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否并发',
                                   `is_update_data` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否更新数据',
                                   `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否接受恢复执行',
                                   `job_data` blob NULL COMMENT '存放持久化job对象',
                                   PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '任务详细信息表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
                             `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
                             `lock_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '悲观锁名称',
                             PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '存储的悲观锁信息表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
                                           `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
                                           `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
                                           PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '暂停的触发器表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
                                       `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
                                       `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '实例名称',
                                       `last_checkin_time` bigint(13) NOT NULL COMMENT '上次检查时间',
                                       `checkin_interval` bigint(13) NOT NULL COMMENT '检查间隔时间',
                                       PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '调度器状态表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
                                       `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
                                       `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
                                       `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
                                       `repeat_count` bigint(7) NOT NULL COMMENT '重复的次数统计',
                                       `repeat_interval` bigint(12) NOT NULL COMMENT '重复的间隔时间',
                                       `times_triggered` bigint(10) NOT NULL COMMENT '已经触发的次数',
                                       PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
                                       CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '简单触发器的信息表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
                                        `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
                                        `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
                                        `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
                                        `str_prop_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第一个参数',
                                        `str_prop_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第二个参数',
                                        `str_prop_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第三个参数',
                                        `int_prop_1` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第一个参数',
                                        `int_prop_2` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第二个参数',
                                        `long_prop_1` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第一个参数',
                                        `long_prop_2` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第二个参数',
                                        `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第一个参数',
                                        `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第二个参数',
                                        `bool_prop_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第一个参数',
                                        `bool_prop_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第二个参数',
                                        PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
                                        CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '同步机制的行锁表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
                                `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
                                `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '触发器的名字',
                                `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '触发器所属组的名字',
                                `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_job_details表job_name的外键',
                                `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_job_details表job_group的外键',
                                `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '相关介绍',
                                `next_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '上一次触发时间（毫秒）',
                                `prev_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '下一次触发时间（默认为-1表示不触发）',
                                `priority` int(11) NULL DEFAULT NULL COMMENT '优先级',
                                `trigger_state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '触发器状态',
                                `trigger_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '触发器的类型',
                                `start_time` bigint(13) NOT NULL COMMENT '开始时间',
                                `end_time` bigint(13) NULL DEFAULT NULL COMMENT '结束时间',
                                `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日程表名称',
                                `misfire_instr` smallint(2) NULL DEFAULT NULL COMMENT '补偿执行的策略',
                                `job_data` blob NULL COMMENT '存放持久化job对象',
                                PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
                                INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
                                CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '触发器详细信息表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
                             `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
                             `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数名称',
                             `config_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键名',
                             `config_value` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键值',
                             `config_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
                             `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                             `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                             `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                             `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                             `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                             `tenant_id` bigint(20) NULL DEFAULT NULL,
                             PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参数配置表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2022-07-21 20:17:34', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow', 0);
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2022-07-21 20:17:34', '', NULL, '初始化密码 123456', 0);
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2022-07-21 20:17:34', '', NULL, '深黑主题theme-dark，浅色主题theme-light，深蓝主题theme-blue', 0);
INSERT INTO `sys_config` VALUES (4, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'true', 'Y', 'admin', '2022-07-21 20:17:34', 'admin', '2022-07-26 15:28:00', '是否开启注册用户功能（true开启，false关闭）', 0);
INSERT INTO `sys_config` VALUES (5, '用户管理-密码字符范围', 'sys.account.chrtype', '0', 'Y', 'admin', '2022-07-21 20:17:34', '', NULL, '默认任意字符范围，0任意（密码可以输入任意字符），1数字（密码只能为0-9数字），2英文字母（密码只能为a-z和A-Z字母），3字母和数字（密码必须包含字母，数字）,4字母数字和特殊字符（目前支持的特殊字符包括：~!@#$%^&*()-=_+）', 0);
INSERT INTO `sys_config` VALUES (6, '用户管理-初始密码修改策略', 'sys.account.initPasswordModify', '0', 'Y', 'admin', '2022-07-21 20:17:34', '', NULL, '0：初始密码修改策略关闭，没有任何提示，1：提醒用户，如果未修改初始密码，则在登录时就会提醒修改密码对话框', 0);
INSERT INTO `sys_config` VALUES (7, '用户管理-账号密码更新周期', 'sys.account.passwordValidateDays', '0', 'Y', 'admin', '2022-07-21 20:17:34', '', NULL, '密码更新周期（填写数字，数据初始化值为0不限制，若修改必须为大于0小于365的正整数），如果超过这个周期登录系统时，则在登录时就会提醒修改密码对话框', 0);
INSERT INTO `sys_config` VALUES (8, '主框架页-菜单导航显示风格', 'sys.index.menuStyle', 'default', 'Y', 'admin', '2022-07-21 20:17:34', '', NULL, '菜单导航显示风格（default为左侧导航菜单，topnav为顶部导航菜单）', 0);
INSERT INTO `sys_config` VALUES (9, '主框架页-是否开启页脚', 'sys.index.footer', 'true', 'Y', 'admin', '2022-07-21 20:17:34', '', NULL, '是否开启底部页脚显示（true显示，false隐藏）', 0);
INSERT INTO `sys_config` VALUES (10, '主框架页-是否开启页签', 'sys.index.tagsView', 'true', 'Y', 'admin', '2022-07-21 20:17:34', '', NULL, '是否开启菜单多页签显示（true显示，false隐藏）', 0);

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
                           `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
                           `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
                           `ancestors` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '祖级列表',
                           `dept_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
                           `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
                           `leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '负责人',
                           `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
                           `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
                           `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
                           `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
                           `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                           `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                           `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                           `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                           `tenant_id` bigint(20) NULL DEFAULT NULL,
                           PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 129 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '腾讯科技', 0, '腾讯', '15888888888', 'tengxun@qq.com', '0', '0', 'admin', '2022-07-21 20:17:34', 'admin', '2022-07-31 22:32:24', 1);
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '深圳总公司', 1, '腾讯', '15888888888', 'tengxun@qq.com', '0', '0', 'admin', '2022-07-21 20:17:34', '', NULL, 1);
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '腾讯', '15888888888', 'tengxun@qq.com', '0', '0', 'admin', '2022-07-21 20:17:34', '', NULL, 1);
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '腾讯', '15888888888', 'tengxun@qq.com', '0', '0', 'admin', '2022-07-21 20:17:34', '', NULL, 1);
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '腾讯', '15888888888', 'tengxun@qq.com', '0', '0', 'admin', '2022-07-21 20:17:34', '', NULL, 1);
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '腾讯', '15888888888', 'tengxun@qq.com', '0', '0', 'admin', '2022-07-21 20:17:34', '', NULL, 1);
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '腾讯', '15888888888', 'tengxun@qq.com', '0', '0', 'admin', '2022-07-21 20:17:34', '', NULL, 1);
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '腾讯', '15888888888', 'tengxun@qq.com', '0', '0', 'admin', '2022-07-21 20:17:34', 'baidu', '2022-08-02 14:55:09', 1);
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '腾讯', '15888888888', 'tengxun@qq.com', '0', '0', 'admin', '2022-07-21 20:17:34', '', NULL, 1);
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '腾讯', '15888888888', 'tengxun@qq.com', '0', '0', 'admin', '2022-07-21 20:17:34', '', NULL, 1);
INSERT INTO `sys_dept` VALUES (110, 0, '0', '百度科技', 0, '百度', '15888888888', 'tengxun@qq.com', '0', '0', 'admin', '2022-07-21 20:17:34', 'admin', '2022-07-31 22:32:24', 2);
INSERT INTO `sys_dept` VALUES (118, 110, '0,110', '测试部门', 1, '百度', NULL, NULL, '0', '0', 'tengxun1', '2022-08-02 18:10:20', '', NULL, 2);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
                                `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
                                `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
                                `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典标签',
                                `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典键值',
                                `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
                                `css_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
                                `list_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
                                `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
                                `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
                                `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                                `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                                `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                `tenant_id` bigint(20) NULL DEFAULT NULL,
                                PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '性别男', 0);
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '性别女', 0);
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '性别未知', 0);
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '显示菜单', 0);
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '隐藏菜单', 0);
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '正常状态', 0);
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '停用状态', 0);
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '正常状态', 0);
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '停用状态', 0);
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '默认分组', 0);
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '系统分组', 0);
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '系统默认是', 0);
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '系统默认否', 0);
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '通知', 0);
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '公告', 0);
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '正常状态', 0);
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '关闭状态', 0);
INSERT INTO `sys_dict_data` VALUES (18, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '其他操作', 0);
INSERT INTO `sys_dict_data` VALUES (19, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '新增操作', 0);
INSERT INTO `sys_dict_data` VALUES (20, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '修改操作', 0);
INSERT INTO `sys_dict_data` VALUES (21, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '删除操作', 0);
INSERT INTO `sys_dict_data` VALUES (22, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '授权操作', 0);
INSERT INTO `sys_dict_data` VALUES (23, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '导出操作', 0);
INSERT INTO `sys_dict_data` VALUES (24, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '导入操作', 0);
INSERT INTO `sys_dict_data` VALUES (25, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '强退操作', 0);
INSERT INTO `sys_dict_data` VALUES (26, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '生成操作', 0);
INSERT INTO `sys_dict_data` VALUES (27, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '清空操作', 0);
INSERT INTO `sys_dict_data` VALUES (28, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '正常状态', 0);
INSERT INTO `sys_dict_data` VALUES (29, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '停用状态', 0);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
                                `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
                                `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典名称',
                                `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
                                `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
                                `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                                `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                                `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                `tenant_id` bigint(20) NULL DEFAULT NULL,
                                PRIMARY KEY (`dict_id`) USING BTREE,
                                UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '用户性别列表', 0);
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '菜单状态列表', 0);
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '系统开关列表', 0);
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '任务状态列表', 0);
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '任务分组列表', 0);
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '系统是否列表', 0);
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '通知类型列表', 0);
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '通知状态列表', 0);
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '操作类型列表', 0);
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '登录状态列表', 0);

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
                          `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
                          `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
                          `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
                          `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
                          `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
                          `misfire_policy` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
                          `concurrent` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
                          `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
                          `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                          `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                          `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                          `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                          `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注信息',
                          PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2022-07-21 20:17:34', '', NULL, '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2022-07-21 20:17:34', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
                              `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
                              `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务名称',
                              `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务组名',
                              `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
                              `job_message` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志信息',
                              `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
                              `exception_info` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '异常信息',
                              `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                              PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
                                 `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
                                 `login_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录账号',
                                 `ipaddr` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
                                 `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录地点',
                                 `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
                                 `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作系统',
                                 `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
                                 `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '提示消息',
                                 `login_time` datetime NULL DEFAULT NULL COMMENT '访问时间',
                                 `tenant_id` bigint(20) NULL DEFAULT NULL,
                                 PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 670 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统访问记录' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (100, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2022-07-21 20:25:00', 1);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
                           `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
                           `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
                           `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
                           `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
                           `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '请求地址',
                           `target` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '打开方式（menuItem页签 menuBlank新窗口）',
                           `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
                           `visible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
                           `is_refresh` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否刷新（0刷新 1不刷新）',
                           `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
                           `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
                           `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                           `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                           `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                           `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                           `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
                           `tenant_id` bigint(20) NULL DEFAULT NULL,
                           PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2020 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 3, '#', 'menuItem', 'M', '0', '1', '', 'fa fa-gear', 'admin', '2022-07-21 20:17:34', 'admin', '2022-08-01 17:16:15', '系统管理目录', 0);
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 4, '#', 'menuItem', 'M', '0', '1', '', 'fa fa-video-camera', 'admin', '2022-07-21 20:17:34', 'admin', '2022-08-01 17:16:20', '系统监控目录', 0);
INSERT INTO `sys_menu` VALUES (3, '基础资料', 0, 1, '#', 'menuItem', 'M', '0', '1', NULL, 'fa fa-cubes', 'admin', '2022-07-31 08:14:04', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, '/system/user', '', 'C', '0', '1', 'system:user:view', 'fa fa-user-o', 'admin', '2022-07-21 20:17:34', '', NULL, '用户管理菜单', 0);
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, '/system/role', '', 'C', '0', '1', 'system:role:view', 'fa fa-user-secret', 'admin', '2022-07-21 20:17:34', '', NULL, '角色管理菜单', 0);
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 6, '/system/menu', 'menuItem', 'C', '0', '1', 'system:menu:view', 'fa fa-th-list', 'admin', '2022-07-21 20:17:34', 'baidu', '2022-08-02 07:58:32', '菜单管理菜单', 0);
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 3, '/system/dept', 'menuItem', 'C', '0', '1', 'system:dept:view', 'fa fa-outdent', 'admin', '2022-07-21 20:17:34', 'baidu', '2022-08-02 07:57:03', '部门管理菜单', 0);
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 4, '/system/post', 'menuItem', 'C', '0', '1', 'system:post:view', 'fa fa-address-card-o', 'admin', '2022-07-21 20:17:34', 'baidu', '2022-08-02 07:57:24', '岗位管理菜单', 0);
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 7, '/system/dict', 'menuItem', 'C', '0', '1', 'system:dict:view', 'fa fa-bookmark-o', 'admin', '2022-07-21 20:17:34', 'baidu', '2022-08-02 07:58:21', '字典管理菜单', 0);
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 8, '/system/config', 'menuItem', 'C', '0', '1', 'system:config:view', 'fa fa-sun-o', 'admin', '2022-07-21 20:17:34', 'baidu', '2022-08-02 07:58:11', '参数设置菜单', 0);
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 5, '/system/notice', 'menuItem', 'C', '0', '1', 'system:notice:view', 'fa fa-bullhorn', 'admin', '2022-07-21 20:17:34', 'baidu', '2022-08-02 07:57:49', '通知公告菜单', 0);
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, '#', '', 'M', '0', '1', '', 'fa fa-pencil-square-o', 'admin', '2022-07-21 20:17:34', '', NULL, '日志管理菜单', 0);
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, '/monitor/online', '', 'C', '0', '1', 'monitor:online:view', 'fa fa-user-circle', 'admin', '2022-07-21 20:17:34', '', NULL, '在线用户菜单', 0);
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, '/monitor/job', '', 'C', '0', '1', 'monitor:job:view', 'fa fa-tasks', 'admin', '2022-07-21 20:17:34', '', NULL, '定时任务菜单', 0);
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, '/monitor/data', '', 'C', '0', '1', 'monitor:data:view', 'fa fa-bug', 'admin', '2022-07-21 20:17:34', '', NULL, '数据监控菜单', 0);
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, '/monitor/operlog', '', 'C', '0', '1', 'monitor:operlog:view', 'fa fa-address-book', 'admin', '2022-07-21 20:17:34', '', NULL, '操作日志菜单', 0);
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, '/monitor/logininfor', '', 'C', '0', '1', 'monitor:logininfor:view', 'fa fa-file-image-o', 'admin', '2022-07-21 20:17:34', '', NULL, '登录日志菜单', 0);
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, '#', '', 'F', '0', '1', 'system:user:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, '#', '', 'F', '0', '1', 'system:user:add', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, '#', '', 'F', '0', '1', 'system:user:edit', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, '#', '', 'F', '0', '1', 'system:user:remove', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1004, '用户导出', 100, 5, '#', '', 'F', '0', '1', 'system:user:export', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1005, '用户导入', 100, 6, '#', '', 'F', '0', '1', 'system:user:import', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1006, '重置密码', 100, 7, '#', '', 'F', '0', '1', 'system:user:resetPwd', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1007, '角色查询', 101, 1, '#', '', 'F', '0', '1', 'system:role:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1008, '角色新增', 101, 2, '#', '', 'F', '0', '1', 'system:role:add', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1009, '角色修改', 101, 3, '#', '', 'F', '0', '1', 'system:role:edit', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1010, '角色删除', 101, 4, '#', '', 'F', '0', '1', 'system:role:remove', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1011, '角色导出', 101, 5, '#', '', 'F', '0', '1', 'system:role:export', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1012, '菜单查询', 102, 1, '#', '', 'F', '0', '1', 'system:menu:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1013, '菜单新增', 102, 2, '#', '', 'F', '0', '1', 'system:menu:add', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1014, '菜单修改', 102, 3, '#', '', 'F', '0', '1', 'system:menu:edit', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1015, '菜单删除', 102, 4, '#', '', 'F', '0', '1', 'system:menu:remove', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1016, '部门查询', 103, 1, '#', '', 'F', '0', '1', 'system:dept:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1017, '部门新增', 103, 2, '#', '', 'F', '0', '1', 'system:dept:add', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1018, '部门修改', 103, 3, '#', '', 'F', '0', '1', 'system:dept:edit', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1019, '部门删除', 103, 4, '#', '', 'F', '0', '1', 'system:dept:remove', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1020, '岗位查询', 104, 1, '#', '', 'F', '0', '1', 'system:post:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1021, '岗位新增', 104, 2, '#', '', 'F', '0', '1', 'system:post:add', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1022, '岗位修改', 104, 3, '#', '', 'F', '0', '1', 'system:post:edit', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1023, '岗位删除', 104, 4, '#', '', 'F', '0', '1', 'system:post:remove', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1024, '岗位导出', 104, 5, '#', '', 'F', '0', '1', 'system:post:export', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1025, '字典查询', 105, 1, '#', '', 'F', '0', '1', 'system:dict:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1026, '字典新增', 105, 2, '#', '', 'F', '0', '1', 'system:dict:add', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1027, '字典修改', 105, 3, '#', '', 'F', '0', '1', 'system:dict:edit', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1028, '字典删除', 105, 4, '#', '', 'F', '0', '1', 'system:dict:remove', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1029, '字典导出', 105, 5, '#', '', 'F', '0', '1', 'system:dict:export', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1030, '参数查询', 106, 1, '#', '', 'F', '0', '1', 'system:config:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1031, '参数新增', 106, 2, '#', '', 'F', '0', '1', 'system:config:add', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1032, '参数修改', 106, 3, '#', '', 'F', '0', '1', 'system:config:edit', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1033, '参数删除', 106, 4, '#', '', 'F', '0', '1', 'system:config:remove', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1034, '参数导出', 106, 5, '#', '', 'F', '0', '1', 'system:config:export', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1035, '公告查询', 107, 1, '#', '', 'F', '0', '1', 'system:notice:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1036, '公告新增', 107, 2, '#', '', 'F', '0', '1', 'system:notice:add', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1037, '公告修改', 107, 3, '#', '', 'F', '0', '1', 'system:notice:edit', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1038, '公告删除', 107, 4, '#', '', 'F', '0', '1', 'system:notice:remove', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1039, '操作查询', 500, 1, '#', '', 'F', '0', '1', 'monitor:operlog:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1040, '操作删除', 500, 2, '#', '', 'F', '0', '1', 'monitor:operlog:remove', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1041, '详细信息', 500, 3, '#', '', 'F', '0', '1', 'monitor:operlog:detail', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 500, 4, '#', '', 'F', '0', '1', 'monitor:operlog:export', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 501, 1, '#', '', 'F', '0', '1', 'monitor:logininfor:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 501, 2, '#', '', 'F', '0', '1', 'monitor:logininfor:remove', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 501, 3, '#', '', 'F', '0', '1', 'monitor:logininfor:export', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1046, '账户解锁', 501, 4, '#', '', 'F', '0', '1', 'monitor:logininfor:unlock', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1047, '在线查询', 109, 1, '#', '', 'F', '0', '1', 'monitor:online:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1048, '批量强退', 109, 2, '#', '', 'F', '0', '1', 'monitor:online:batchForceLogout', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1049, '单条强退', 109, 3, '#', '', 'F', '0', '1', 'monitor:online:forceLogout', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1050, '任务查询', 110, 1, '#', '', 'F', '0', '1', 'monitor:job:list', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1051, '任务新增', 110, 2, '#', '', 'F', '0', '1', 'monitor:job:add', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1052, '任务修改', 110, 3, '#', '', 'F', '0', '1', 'monitor:job:edit', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1053, '任务删除', 110, 4, '#', '', 'F', '0', '1', 'monitor:job:remove', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1054, '状态修改', 110, 5, '#', '', 'F', '0', '1', 'monitor:job:changeStatus', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1055, '任务详细', 110, 6, '#', '', 'F', '0', '1', 'monitor:job:detail', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (1056, '任务导出', 110, 7, '#', '', 'F', '0', '1', 'monitor:job:export', '#', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (2011, '名片管理', 3, 1, '/base/businesscard', '', 'C', '0', '1', 'base:businesscard:view', '#', 'admin', '2022-07-31 22:25:23', '', NULL, '名片管理菜单', 0);
INSERT INTO `sys_menu` VALUES (2012, '名片管理查询', 2011, 1, '#', '', 'F', '0', '1', 'base:businesscard:list', '#', 'admin', '2022-07-31 22:25:23', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (2013, '名片管理新增', 2011, 2, '#', '', 'F', '0', '1', 'base:businesscard:add', '#', 'admin', '2022-07-31 22:25:23', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (2014, '名片管理修改', 2011, 3, '#', '', 'F', '0', '1', 'base:businesscard:edit', '#', 'admin', '2022-07-31 22:25:23', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (2015, '名片管理删除', 2011, 4, '#', '', 'F', '0', '1', 'base:businesscard:remove', '#', 'admin', '2022-07-31 22:25:23', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (2016, '名片管理导出', 2011, 5, '#', '', 'F', '0', '1', 'base:businesscard:export', '#', 'admin', '2022-07-31 22:25:23', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (2017, '系统设置', 0, 2, '#', 'menuItem', 'M', '0', '1', NULL, 'fa fa-cogs', 'manager', '2022-08-01 17:15:07', '', NULL, '', 0);
INSERT INTO `sys_menu` VALUES (2019, '账本设置', 2017, 2, '/system/tenant/profile', 'menuItem', 'C', '0', '1', '', '#', 'admin', '2022-08-01 17:17:14', 'admin', '2022-08-01 18:03:25', '', 0);

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
                             `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
                             `notice_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告标题',
                             `notice_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
                             `notice_content` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公告内容',
                             `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
                             `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                             `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                             `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                             `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                             `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                             `tenant_id` bigint(20) NULL DEFAULT NULL,
                             PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '通知公告表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 新版本发布啦', '2', '新版本内容', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '管理员', 1);
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 系统凌晨维护', '1', '维护内容', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '管理员', 1);
INSERT INTO `sys_notice` VALUES (3, '测试公告', '1', '<p>测试公告<br></p>', '0', 'baidu', '2022-08-02 17:52:43', 'tx', '2022-08-02 18:25:07', NULL, 1);

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
                               `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
                               `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '模块标题',
                               `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
                               `method` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '方法名称',
                               `request_method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求方式',
                               `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
                               `oper_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作人员',
                               `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
                               `oper_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求URL',
                               `oper_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '主机地址',
                               `oper_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作地点',
                               `oper_param` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求参数',
                               `json_result` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '返回参数',
                               `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
                               `error_msg` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '错误消息',
                               `oper_time` datetime NULL DEFAULT NULL COMMENT '操作时间',
                               `tenant_id` bigint(20) NULL DEFAULT NULL,
                               PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 227 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志记录' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (170, '用户管理', 1, 'com.langmeng.web.controller.system.SysUserController.addSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/user/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"\"],\"userName\":[\"baidu2\"],\"deptName\":[\"\"],\"phonenumber\":[\"\"],\"email\":[\"\"],\"loginName\":[\"baidu2\"],\"sex\":[\"1\"],\"role\":[\"3\"],\"remark\":[\"1111\"],\"status\":[\"0\"],\"roleIds\":[\"3\"],\"postIds\":[\"2\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 09:39:32', NULL);
INSERT INTO `sys_oper_log` VALUES (171, '重置密码', 2, 'com.langmeng.web.controller.system.SysUserController.resetPwdSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/user/resetPwd', '127.0.0.1', '内网IP', '{\"userId\":[\"7\"],\"loginName\":[\"baidu2\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 09:39:57', NULL);
INSERT INTO `sys_oper_log` VALUES (172, '用户管理', 2, 'com.langmeng.web.controller.system.SysUserController.editSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/user/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"7\"],\"deptId\":[\"\"],\"userName\":[\"baidu2\"],\"dept.deptName\":[\"\"],\"phonenumber\":[\"\"],\"email\":[\"\"],\"loginName\":[\"baidu2\"],\"sex\":[\"1\"],\"role\":[\"4\"],\"remark\":[\"1111\"],\"status\":[\"0\"],\"roleIds\":[\"4\"],\"postIds\":[\"2\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 09:41:00', NULL);
INSERT INTO `sys_oper_log` VALUES (173, '角色管理', 1, 'com.langmeng.web.controller.system.SysRoleController.addSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/role/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"百度角色\"],\"roleKey\":[\"baidurole\"],\"roleSort\":[\"5\"],\"status\":[\"0\"],\"remark\":[\"\"],\"menuIds\":[\"3,2011,2012,2013,2014,2015,2016\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 09:42:51', NULL);
INSERT INTO `sys_oper_log` VALUES (174, '角色管理', 1, 'com.langmeng.web.controller.system.SysRoleController.addSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/role/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"百度角色2\"],\"roleKey\":[\"baidu2\"],\"roleSort\":[\"6\"],\"status\":[\"0\"],\"remark\":[\"\"],\"menuIds\":[\"3,2011,2012,2013,2014,2015,2016,2017,2019\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 10:37:13', NULL);
INSERT INTO `sys_oper_log` VALUES (175, '用户管理', 1, 'com.langmeng.web.controller.system.SysUserController.addSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/user/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"104\"],\"userName\":[\"baidu3\"],\"deptName\":[\"市场部门\"],\"phonenumber\":[\"\"],\"email\":[\"\"],\"loginName\":[\"baidu3\"],\"sex\":[\"0\"],\"role\":[\"6\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"6\"],\"postIds\":[\"\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 10:38:09', NULL);
INSERT INTO `sys_oper_log` VALUES (176, '用户管理', 2, 'com.langmeng.web.controller.system.SysUserController.editSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/user/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"8\"],\"deptId\":[\"104\"],\"userName\":[\"baidu3\"],\"dept.deptName\":[\"市场部门\"],\"phonenumber\":[\"\"],\"email\":[\"\"],\"loginName\":[\"baidu3\"],\"sex\":[\"0\"],\"role\":[\"6\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"6\"],\"postIds\":[\"3\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 10:39:07', NULL);
INSERT INTO `sys_oper_log` VALUES (177, '角色管理', 4, 'com.langmeng.web.controller.system.SysRoleController.selectAuthUserAll()', 'POST', 1, 'baidu', '腾讯科技', '/system/role/authUser/selectAll', '127.0.0.1', '内网IP', '{\"roleId\":[\"6\"],\"userIds\":[\"7\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 10:40:38', NULL);
INSERT INTO `sys_oper_log` VALUES (178, '用户管理', 2, 'com.langmeng.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/user/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"4\"],\"deptId\":[\"100\"],\"userName\":[\"租户百度1\"],\"dept.deptName\":[\"腾讯科技\"],\"phonenumber\":[\"\"],\"email\":[\"baidu@qq.com\"],\"loginName\":[\"baidu\"],\"sex\":[\"1\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"\"],\"postIds\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 10:49:15', NULL);
INSERT INTO `sys_oper_log` VALUES (179, '用户管理', 1, 'com.langmeng.web.controller.system.SysUserController.addSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/user/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"\"],\"userName\":[\"baidu2\"],\"deptName\":[\"\"],\"phonenumber\":[\"\"],\"email\":[\"\"],\"loginName\":[\"baidu2\"],\"sex\":[\"0\"],\"role\":[\"3\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"3\"],\"postIds\":[\"\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 11:29:13', NULL);
INSERT INTO `sys_oper_log` VALUES (180, '重置密码', 2, 'com.langmeng.web.controller.system.SysUserController.resetPwdSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/user/resetPwd', '127.0.0.1', '内网IP', '{\"userId\":[\"9\"],\"loginName\":[\"baidu2\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 11:30:11', NULL);
INSERT INTO `sys_oper_log` VALUES (181, '角色管理', 1, 'com.langmeng.web.controller.system.SysRoleController.addSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/role/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"测试角色\"],\"roleKey\":[\"baidu2\"],\"roleSort\":[\"4\"],\"status\":[\"0\"],\"remark\":[\"反反复复\"],\"menuIds\":[\"3,2011,2012,2013,2014,2015,2016\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 11:34:56', NULL);
INSERT INTO `sys_oper_log` VALUES (182, '用户管理', 2, 'com.langmeng.web.controller.system.SysUserController.editSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/user/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"9\"],\"deptId\":[\"\"],\"userName\":[\"baidu2\"],\"dept.deptName\":[\"\"],\"phonenumber\":[\"\"],\"email\":[\"\"],\"loginName\":[\"baidu2\"],\"sex\":[\"0\"],\"role\":[\"3\",\"7\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"3,7\"],\"postIds\":[\"\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 11:35:31', NULL);
INSERT INTO `sys_oper_log` VALUES (183, '岗位管理', 1, 'com.langmeng.web.controller.system.SysPostController.addSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/post/add', '127.0.0.1', '内网IP', '{\"postName\":[\"测试岗位\"],\"postCode\":[\"test\"],\"postSort\":[\"2\"],\"status\":[\"0\"],\"remark\":[\"\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 14:10:03', NULL);
INSERT INTO `sys_oper_log` VALUES (184, '岗位管理', 1, 'com.langmeng.web.controller.system.SysPostController.addSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/post/add', '127.0.0.1', '内网IP', '{\"postName\":[\"测试岗位\"],\"postCode\":[\"3\"],\"postSort\":[\"3\"],\"status\":[\"0\"],\"remark\":[\"333\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 14:12:39', NULL);
INSERT INTO `sys_oper_log` VALUES (185, '岗位管理', 2, 'com.langmeng.web.controller.system.SysPostController.editSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/post/edit', '127.0.0.1', '内网IP', '{\"postId\":[\"6\"],\"postName\":[\"测试岗位\"],\"postCode\":[\"3\"],\"postSort\":[\"3\"],\"status\":[\"0\"],\"remark\":[\"333444\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 14:12:58', NULL);
INSERT INTO `sys_oper_log` VALUES (186, '用户管理', 2, 'com.langmeng.web.controller.system.SysUserController.editSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/user/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"9\"],\"deptId\":[\"\"],\"userName\":[\"baidu2\"],\"dept.deptName\":[\"\"],\"phonenumber\":[\"\"],\"email\":[\"\"],\"loginName\":[\"baidu2\"],\"sex\":[\"0\"],\"role\":[\"3\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"3\"],\"postIds\":[\"6\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 14:13:22', NULL);
INSERT INTO `sys_oper_log` VALUES (187, '用户管理', 2, 'com.langmeng.web.controller.system.SysUserController.editSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/user/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"9\"],\"deptId\":[\"\"],\"userName\":[\"baidu2\"],\"dept.deptName\":[\"\"],\"phonenumber\":[\"\"],\"email\":[\"\"],\"loginName\":[\"baidu2\"],\"sex\":[\"0\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"\"],\"postIds\":[\"6\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 14:14:32', NULL);
INSERT INTO `sys_oper_log` VALUES (188, '用户管理', 2, 'com.langmeng.web.controller.system.SysUserController.editSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/user/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"9\"],\"deptId\":[\"\"],\"userName\":[\"baidu2\"],\"dept.deptName\":[\"\"],\"phonenumber\":[\"\"],\"email\":[\"\"],\"loginName\":[\"baidu2\"],\"sex\":[\"0\"],\"role\":[\"7\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"7\"],\"postIds\":[\"6\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 14:14:48', NULL);
INSERT INTO `sys_oper_log` VALUES (189, '用户管理', 2, 'com.langmeng.web.controller.system.SysUserController.editSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/user/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"9\"],\"deptId\":[\"\"],\"userName\":[\"baidu2\"],\"dept.deptName\":[\"\"],\"phonenumber\":[\"\"],\"email\":[\"\"],\"loginName\":[\"baidu2\"],\"sex\":[\"0\"],\"role\":[\"7\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"7\"],\"postIds\":[\"1,6\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 14:16:29', NULL);
INSERT INTO `sys_oper_log` VALUES (190, '用户管理', 2, 'com.langmeng.web.controller.system.SysUserController.editSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/user/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"9\"],\"deptId\":[\"\"],\"userName\":[\"baidu2\"],\"dept.deptName\":[\"\"],\"phonenumber\":[\"\"],\"email\":[\"\"],\"loginName\":[\"baidu2\"],\"sex\":[\"0\"],\"role\":[\"7\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"7\"],\"postIds\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 14:20:58', NULL);
INSERT INTO `sys_oper_log` VALUES (191, '用户管理', 2, 'com.langmeng.web.controller.system.SysUserController.editSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/user/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"9\"],\"deptId\":[\"\"],\"userName\":[\"baidu2\"],\"dept.deptName\":[\"\"],\"phonenumber\":[\"\"],\"email\":[\"\"],\"loginName\":[\"baidu2\"],\"sex\":[\"0\"],\"role\":[\"7\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"7\"],\"postIds\":[\"1,6\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 14:21:07', NULL);
INSERT INTO `sys_oper_log` VALUES (192, '用户管理', 2, 'com.langmeng.web.controller.system.SysUserController.editSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/user/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"9\"],\"deptId\":[\"\"],\"userName\":[\"baidu2\"],\"dept.deptName\":[\"\"],\"phonenumber\":[\"\"],\"email\":[\"\"],\"loginName\":[\"baidu2\"],\"sex\":[\"0\"],\"role\":[\"7\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"7\"],\"postIds\":[\"\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 14:21:17', NULL);
INSERT INTO `sys_oper_log` VALUES (193, '用户管理', 2, 'com.langmeng.web.controller.system.SysUserController.editSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/user/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"9\"],\"deptId\":[\"\"],\"userName\":[\"baidu2\"],\"dept.deptName\":[\"\"],\"phonenumber\":[\"\"],\"email\":[\"\"],\"loginName\":[\"baidu2\"],\"sex\":[\"0\"],\"role\":[\"7\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"7\"],\"postIds\":[\"6\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 14:21:24', NULL);
INSERT INTO `sys_oper_log` VALUES (194, '岗位管理', 2, 'com.langmeng.web.controller.system.SysPostController.editSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/post/edit', '127.0.0.1', '内网IP', '{\"postId\":[\"6\"],\"postName\":[\"测试岗位\"],\"postCode\":[\"322\"],\"postSort\":[\"333\"],\"status\":[\"0\"],\"remark\":[\"333444\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 14:22:10', NULL);
INSERT INTO `sys_oper_log` VALUES (195, '岗位管理', 3, 'com.langmeng.web.controller.system.SysPostController.remove()', 'POST', 1, 'baidu', '腾讯科技', '/system/post/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"6\"]}', '{\"msg\":\"测试岗位已分配,不能删除\",\"code\":500}', 0, NULL, '2022-08-02 14:22:23', NULL);
INSERT INTO `sys_oper_log` VALUES (196, '岗位管理', 3, 'com.langmeng.web.controller.system.SysPostController.remove()', 'POST', 1, 'baidu', '腾讯科技', '/system/post/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"6\"]}', '{\"msg\":\"测试岗位已分配,不能删除\",\"code\":500}', 0, NULL, '2022-08-02 14:22:30', NULL);
INSERT INTO `sys_oper_log` VALUES (197, '部门管理', 2, 'com.langmeng.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/dept/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"107\"],\"parentId\":[\"101\"],\"parentName\":[\"深圳总公司\"],\"deptName\":[\"运维部门\"],\"orderNum\":[\"5\"],\"leader\":[\"腾讯\"],\"phone\":[\"15888888888\"],\"email\":[\"tengxun@qq.com\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 14:55:09', NULL);
INSERT INTO `sys_oper_log` VALUES (198, '部门管理', 1, 'com.langmeng.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"测试公司\"],\"orderNum\":[\"99\"],\"leader\":[\"23\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 14:59:14', NULL);
INSERT INTO `sys_oper_log` VALUES (199, '部门管理', 1, 'com.langmeng.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"巢成雏长大\"],\"orderNum\":[\"22\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 15:03:09', NULL);
INSERT INTO `sys_oper_log` VALUES (200, '部门管理', 1, 'com.langmeng.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"345345\"],\"orderNum\":[\"333\"],\"leader\":[\"34534\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 15:08:43', NULL);
INSERT INTO `sys_oper_log` VALUES (201, '部门管理', 1, 'com.langmeng.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"测试公司\"],\"orderNum\":[\"5\"],\"leader\":[\"腾讯\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 15:09:23', NULL);
INSERT INTO `sys_oper_log` VALUES (202, '部门管理', 2, 'com.langmeng.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/dept/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"114\"],\"parentId\":[\"100\"],\"parentName\":[\"腾讯科技\"],\"deptName\":[\"测试公司1\"],\"orderNum\":[\"5\"],\"leader\":[\"腾讯\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 15:09:33', NULL);
INSERT INTO `sys_oper_log` VALUES (203, '部门管理', 1, 'com.langmeng.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"测试2\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 15:09:49', NULL);
INSERT INTO `sys_oper_log` VALUES (204, '部门管理', 2, 'com.langmeng.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/dept/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"114\"],\"parentId\":[\"100\"],\"parentName\":[\"腾讯科技\"],\"deptName\":[\"测试公司1\"],\"orderNum\":[\"5\"],\"leader\":[\"腾讯\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 15:10:38', NULL);
INSERT INTO `sys_oper_log` VALUES (205, '部门管理', 1, 'com.langmeng.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"测试1\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 15:11:34', NULL);
INSERT INTO `sys_oper_log` VALUES (206, '部门管理', 1, 'com.langmeng.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"116\"],\"deptName\":[\"测试2\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 15:11:55', NULL);
INSERT INTO `sys_oper_log` VALUES (207, '个人信息', 2, 'com.langmeng.web.controller.system.SysProfileController.update()', 'POST', 1, 'baidu', '腾讯科技', '/system/user/profile/update', '127.0.0.1', '内网IP', '{\"admin\":false,\"dept\":{\"params\":{}},\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 17:09:29', NULL);
INSERT INTO `sys_oper_log` VALUES (208, '个人信息', 2, 'com.langmeng.web.controller.system.SysProfileController.update()', 'POST', 1, 'baidu', '腾讯科技', '/system/user/profile/update', '127.0.0.1', '内网IP', '{\"admin\":false,\"dept\":{\"params\":{}},\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 17:12:49', NULL);
INSERT INTO `sys_oper_log` VALUES (209, '个人信息', 2, 'com.langmeng.web.controller.system.SysProfileController.update()', 'POST', 1, 'baidu', '腾讯科技', '/system/user/profile/update', '127.0.0.1', '内网IP', '{\"admin\":false,\"dept\":{\"params\":{}},\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 17:17:32', NULL);
INSERT INTO `sys_oper_log` VALUES (210, '个人信息', 2, 'com.langmeng.web.controller.system.SysProfileController.update()', 'POST', 1, 'baidu', '腾讯科技', '/system/user/profile/update', '127.0.0.1', '内网IP', '{\"admin\":false,\"dept\":{\"params\":{}},\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 17:18:57', NULL);
INSERT INTO `sys_oper_log` VALUES (211, '账户信息', 2, 'com.langmeng.web.controller.system.SysTenantProfileController.update()', 'POST', 1, 'baidu', '腾讯科技', '/system/tenant/profile/update', '127.0.0.1', '内网IP', '{\"admin\":false,\"dept\":{\"params\":{}},\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 17:20:45', NULL);
INSERT INTO `sys_oper_log` VALUES (212, '账户信息', 2, 'com.langmeng.web.controller.system.SysTenantProfileController.update()', 'POST', 1, 'baidu', '腾讯科技', '/system/tenant/profile/update', '127.0.0.1', '内网IP', '{\"tenantId\":[\"1\"],\"tenantName\":[\"腾讯2\"],\"tenantNo\":[\"0000000001\"],\"logoUrl\":[\"\"],\"remark\":[\"11\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 17:30:59', NULL);
INSERT INTO `sys_oper_log` VALUES (213, '账户信息', 2, 'com.langmeng.web.controller.system.SysTenantProfileController.update()', 'POST', 1, 'baidu', '腾讯科技', '/system/tenant/profile/update', '127.0.0.1', '内网IP', '{\"tenantId\":[\"1\"],\"tenantName\":[\"腾讯2\"],\"tenantNo\":[\"0000000001\"],\"logoUrl\":[\"33\"],\"remark\":[\"11222\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 17:31:15', NULL);
INSERT INTO `sys_oper_log` VALUES (214, '账户信息', 2, 'com.langmeng.web.controller.system.SysTenantProfileController.update()', 'POST', 1, 'baidu', '腾讯科技', '/system/tenant/profile/update', '127.0.0.1', '内网IP', '{\"tenantId\":[\"1\"],\"tenantName\":[\"腾讯2\"],\"tenantNo\":[\"0000000001\"],\"logoUrl\":[\"33\"],\"remark\":[\"11222\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 17:33:05', NULL);
INSERT INTO `sys_oper_log` VALUES (215, '租户管理', 2, 'com.langmeng.web.controller.platform.PlatformTenantController.editSave()', 'POST', 1, 'admin', NULL, '/platform/tenant/edit', '127.0.0.1', '内网IP', '{\"tenantId\":[\"1\"],\"tenantName\":[\"腾讯2\"],\"tenantNo\":[\"01\"],\"logoUrl\":[\"33\"],\"remark\":[\"11222\"],\"startDate\":[\"2021-08-04\"],\"endDate\":[\"2050-03-07\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 17:41:19', NULL);
INSERT INTO `sys_oper_log` VALUES (216, '通知公告', 1, 'com.langmeng.web.controller.system.SysNoticeController.addSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/notice/add', '127.0.0.1', '内网IP', '{\"noticeTitle\":[\"测试公告\"],\"noticeType\":[\"1\"],\"noticeContent\":[\"<p>测试公告<br></p>\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 17:52:43', NULL);
INSERT INTO `sys_oper_log` VALUES (217, '用户管理', 1, 'com.langmeng.web.controller.system.SysUserController.addSave()', 'POST', 1, 'baidu', '腾讯科技', '/system/user/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"\"],\"userName\":[\"tengxun1\"],\"deptName\":[\"\"],\"phonenumber\":[\"\"],\"email\":[\"\"],\"loginName\":[\"tengxun1\"],\"sex\":[\"0\"],\"role\":[\"2\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"2\"],\"postIds\":[\"\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 18:05:52', NULL);
INSERT INTO `sys_oper_log` VALUES (218, '部门管理', 1, 'com.langmeng.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'tengxun1', '百度科技', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"110\"],\"deptName\":[\"测试部门\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 18:10:20', NULL);
INSERT INTO `sys_oper_log` VALUES (219, '重置密码', 2, 'com.langmeng.web.controller.system.SysUserController.resetPwdSave()', 'POST', 1, 'tengxun1', '百度科技', '/system/user/resetPwd', '127.0.0.1', '内网IP', '{\"userId\":[\"2\"],\"loginName\":[\"tx\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 18:19:04', NULL);
INSERT INTO `sys_oper_log` VALUES (220, '通知公告', 2, 'com.langmeng.web.controller.system.SysNoticeController.editSave()', 'POST', 1, 'tx', '测试部门', '/system/notice/edit', '127.0.0.1', '内网IP', '{\"noticeId\":[\"3\"],\"noticeTitle\":[\"测试公告\"],\"noticeType\":[\"1\"],\"noticeContent\":[\"<p>测试公告<br></p>\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 18:25:07', NULL);
INSERT INTO `sys_oper_log` VALUES (221, '代码生成', 3, 'com.langmeng.generator.controller.GenController.remove()', 'POST', 1, 'admin', NULL, '/tool/gen/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"15\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 20:05:58', NULL);
INSERT INTO `sys_oper_log` VALUES (222, '代码生成', 6, 'com.langmeng.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":[\"base_businesscard\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 20:06:12', NULL);
INSERT INTO `sys_oper_log` VALUES (223, '代码生成', 2, 'com.langmeng.generator.controller.GenController.editSave()', 'POST', 1, 'admin', NULL, '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"16\"],\"tableName\":[\"base_businesscard\"],\"tableComment\":[\"名片管理\"],\"className\":[\"BaseBusinesscard\"],\"functionAuthor\":[\"大猫咪\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"167\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"名片ID\"],\"columns[0].javaType\":[\"Long\"],\"columns[0].javaField\":[\"id\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"168\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"姓名\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"fullName\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"LIKE\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"169\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"公司\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"company\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"170\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"职务\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"title\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"171\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"电话\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"phone\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"172\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"邮编\"],\"columns[5].javaType\":[\"String\"],\"columns[5].javaField\":[\"postalCode\"],\"columns[5].isInse', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 20:07:50', NULL);
INSERT INTO `sys_oper_log` VALUES (224, '代码生成', 8, 'com.langmeng.generator.controller.GenController.download()', 'GET', 1, 'admin', NULL, '/tool/gen/download/base_businesscard', '127.0.0.1', '内网IP', '\"base_businesscard\"', NULL, 0, NULL, '2022-08-02 20:08:01', NULL);
INSERT INTO `sys_oper_log` VALUES (225, '名片管理', 1, 'com.langmeng.base.controller.BaseBusinesscardController.addSave()', 'POST', 1, 'baidu', '百度科技', '/base/businesscard/add', '127.0.0.1', '内网IP', '{\"fullName\":[\"测试\"],\"company\":[\"3423432\"],\"title\":[\"234324\"],\"phone\":[\"234\"],\"postalCode\":[\"\"],\"mobile\":[\"\"],\"address\":[\"\"],\"email\":[\"\"],\"officePhone\":[\"\"],\"qq\":[\"\"],\"fax\":[\"\"],\"web\":[\"\"],\"bankName\":[\"\"],\"bankAccount\":[\"\"],\"taxAccount\":[\"\"],\"personal\":[\"\"],\"deletionstateCode\":[\"\"],\"sortCode\":[\"\"],\"remark\":[\"\"],\"tenantId\":[\"\"]}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in file [D:\\java\\project\\saas-platform\\langmeng-base\\target\\classes\\mapper\\base\\BaseBusinesscardMapper.xml]\r\n### The error may involve com.langmeng.base.mapper.BaseBusinesscardMapper.insertBaseBusinesscard-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into base_businesscard          ( full_name,             company,             title,             phone,             postal_code,             mobile,             address,             email,             office_phone,             qq,             fax,             web,             bank_name,             bank_account,             tax_account,                                                    remark,             tenant_id )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                                                    ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2022-08-02 20:44:35', NULL);
INSERT INTO `sys_oper_log` VALUES (226, '名片管理', 1, 'com.langmeng.base.controller.BaseBusinesscardController.addSave()', 'POST', 1, 'baidu', '百度科技', '/base/businesscard/add', '127.0.0.1', '内网IP', '{\"fullName\":[\"67867\"],\"company\":[\"678\"],\"title\":[\"678\"],\"phone\":[\"\"],\"postalCode\":[\"\"],\"mobile\":[\"\"],\"address\":[\"\"],\"email\":[\"\"],\"officePhone\":[\"\"],\"qq\":[\"\"],\"fax\":[\"\"],\"web\":[\"\"],\"bankName\":[\"\"],\"bankAccount\":[\"\"],\"taxAccount\":[\"\"],\"personal\":[\"\"],\"deletionstateCode\":[\"\"],\"sortCode\":[\"\"],\"remark\":[\"\"],\"tenantId\":[\"\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2022-08-02 20:45:10', NULL);

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
                           `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
                           `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位编码',
                           `post_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位名称',
                           `post_sort` int(4) NOT NULL COMMENT '显示顺序',
                           `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
                           `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                           `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                           `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                           `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                           `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                           `tenant_id` bigint(20) NULL DEFAULT NULL,
                           PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '岗位信息表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2022-07-21 20:17:34', '', NULL, '', 1);
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2022-07-21 20:17:34', '', NULL, '', 0);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
                           `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
                           `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
                           `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色权限字符串',
                           `role_sort` int(4) NOT NULL COMMENT '显示顺序',
                           `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
                           `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
                           `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
                           `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                           `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                           `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                           `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                           `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                           `tenant_id` bigint(20) NULL DEFAULT NULL,
                           PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色信息表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', '0', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '超级管理员', 0);
INSERT INTO `sys_role` VALUES (2, '普通角色(系统内置)', 'common', 2, '1', '0', '0', 'admin', '2022-07-21 20:17:34', '', NULL, '普通角色', 0);
INSERT INTO `sys_role` VALUES (3, '平台管理员', 'manager', 3, '1', '0', '0', 'admin', '2022-08-01 16:43:48', 'admin', '2022-08-01 22:18:21', '平台管理员', 1);
INSERT INTO `sys_role` VALUES (7, '测试角色', 'baidu2', 4, '1', '0', '0', 'baidu', '2022-08-02 11:34:56', '', NULL, '反反复复', 2);

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
                                `role_id` bigint(20) NOT NULL COMMENT '角色ID',
                                `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
                                `tenant_id` bigint(20) NULL DEFAULT NULL,
                                PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 101, 1);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
                                `role_id` bigint(20) NOT NULL COMMENT '角色ID',
                                `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
                                `tenant_id` bigint(20) NULL DEFAULT NULL,
                                PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1, 0);
INSERT INTO `sys_role_menu` VALUES (2, 2, 0);
INSERT INTO `sys_role_menu` VALUES (2, 3, 0);
INSERT INTO `sys_role_menu` VALUES (2, 100, 0);
INSERT INTO `sys_role_menu` VALUES (2, 101, 0);
INSERT INTO `sys_role_menu` VALUES (2, 102, 0);
INSERT INTO `sys_role_menu` VALUES (2, 103, 0);
INSERT INTO `sys_role_menu` VALUES (2, 104, 0);
INSERT INTO `sys_role_menu` VALUES (2, 105, 0);
INSERT INTO `sys_role_menu` VALUES (2, 106, 0);
INSERT INTO `sys_role_menu` VALUES (2, 107, 0);
INSERT INTO `sys_role_menu` VALUES (2, 108, 0);
INSERT INTO `sys_role_menu` VALUES (2, 109, 0);
INSERT INTO `sys_role_menu` VALUES (2, 110, 0);
INSERT INTO `sys_role_menu` VALUES (2, 111, 0);
INSERT INTO `sys_role_menu` VALUES (2, 500, 0);
INSERT INTO `sys_role_menu` VALUES (2, 501, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1000, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1001, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1002, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1003, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1004, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1005, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1006, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1007, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1008, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1009, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1010, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1011, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1012, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1013, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1014, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1015, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1016, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1017, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1018, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1019, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1020, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1021, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1022, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1023, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1024, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1025, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1026, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1027, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1028, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1029, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1030, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1031, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1032, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1033, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1034, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1035, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1036, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1037, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1038, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1039, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1040, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1041, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1042, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1043, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1044, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1045, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1046, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1047, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1048, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1049, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1050, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1051, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1052, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1053, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1054, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1055, 0);
INSERT INTO `sys_role_menu` VALUES (2, 1056, 0);
INSERT INTO `sys_role_menu` VALUES (2, 2011, 0);
INSERT INTO `sys_role_menu` VALUES (2, 2012, 0);
INSERT INTO `sys_role_menu` VALUES (2, 2013, 0);
INSERT INTO `sys_role_menu` VALUES (2, 2014, 0);
INSERT INTO `sys_role_menu` VALUES (2, 2015, 0);
INSERT INTO `sys_role_menu` VALUES (2, 2016, 0);
INSERT INTO `sys_role_menu` VALUES (2, 2017, 0);
INSERT INTO `sys_role_menu` VALUES (2, 2019, 0);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
                           `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
                           `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
                           `login_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录账号',
                           `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户昵称',
                           `user_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户 01注册用户）',
                           `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
                           `phonenumber` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '手机号码',
                           `sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
                           `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '头像路径',
                           `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '密码',
                           `salt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '盐加密',
                           `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
                           `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
                           `login_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
                           `login_date` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
                           `pwd_update_date` datetime NULL DEFAULT NULL COMMENT '密码最后更新时间',
                           `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                           `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                           `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                           `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                           `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                           `tenant_id` bigint(20) NULL DEFAULT NULL,
                           PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '租户管理员', '00', 'zuhu@163.com', '15888888888', '1', '', '29c67a30398638269fe600f73a054934', '111111', '0', '0', '127.0.0.1', '2022-08-02 11:02:45', '2022-07-21 20:17:34', 'admin', '2022-07-21 20:17:34', '', '2022-08-02 11:02:45', '管理员', 0);
INSERT INTO `sys_user` VALUES (2, 105, 'tx', '租户腾讯', '00', 'tengxun@qq.com', '15666666666', '1', '', '4f4a45b4759b8df25002b54df7c2b8a1', '981b5d', '0', '0', '127.0.0.1', '2022-08-02 20:45:37', '2022-07-21 20:17:34', 'admin', '2022-07-21 20:17:34', '', '2022-08-02 20:45:37', '测试员', 1);
INSERT INTO `sys_user` VALUES (4, 110, 'baidu', '租户百度', '00', 'baidu@qq.com', '', '1', '', '2ebd3a8662652f40a06680d40a2326fa', '729c11', '0', '0', '127.0.0.1', '2022-08-02 21:32:25', NULL, 'admin', '2022-08-01 16:47:50', 'admin', '2022-08-02 21:32:25', '', 2);

-- ----------------------------
-- Table structure for sys_user_online
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_online`;
CREATE TABLE `sys_user_online`  (
                                  `sessionId` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户会话id',
                                  `login_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录账号',
                                  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
                                  `ipaddr` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
                                  `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录地点',
                                  `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
                                  `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作系统',
                                  `status` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '在线状态on_line在线off_line离线',
                                  `start_timestamp` datetime NULL DEFAULT NULL COMMENT 'session创建时间',
                                  `last_access_time` datetime NULL DEFAULT NULL COMMENT 'session最后访问时间',
                                  `expire_time` int(5) NULL DEFAULT 0 COMMENT '超时时间，单位为分钟',
                                  `tenant_id` bigint(20) NULL DEFAULT NULL,
                                  PRIMARY KEY (`sessionId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '在线用户记录' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_user_online
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
                                `user_id` bigint(20) NOT NULL COMMENT '用户ID',
                                `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
                                `tenant_id` bigint(20) NULL DEFAULT NULL,
                                PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (4, 1, 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
                                `user_id` bigint(20) NOT NULL COMMENT '用户ID',
                                `role_id` bigint(20) NOT NULL COMMENT '角色ID',
                                `tenant_id` bigint(20) NULL DEFAULT NULL,
                                PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (2, 2, 1);
INSERT INTO `sys_user_role` VALUES (4, 2, 2);

SET FOREIGN_KEY_CHECKS = 1;