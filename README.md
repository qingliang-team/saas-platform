# 琅檬SaaS-Platform

### 介绍
基于SpringBoot的Saas管理系统，核心技术采用Spring、MyBatis、Shiro没有任何其它重度依赖。

### 技术选型
1、系统环境
- Java EE 8
- Servlet 3.0
- Apache Maven 3

2、主框架
- Spring Boot 2.2.x
- Spring Framework 5.2.x
- Apache Shiro 1.7

3、持久层
- Apache MyBatis 3.5.x
- Hibernate Validation 6.0.x
- Alibaba Druid 1.2.x

4、视图层
- Bootstrap 3.3.7
- Thymeleaf 3.0.x

### 环境要求
- jdk或jre 1.8 及以上
- mysql 5.5 及以上

### 内置功能

* 用户管理：用户是系统操作者，该功能主要完成系统用户配置。
* 部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
* 岗位管理：配置系统用户所属担任职务。
* 菜单管理：配置系统菜单，操作权限，按钮权限标识等。
* 角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
* 字典管理：对系统中经常使用的一些较为固定的数据进行维护。
* 参数管理：对系统动态配置常用参数。
---
* 平台管理：租户管理，租户基本资料管理。
---
* 基础资料：名片管理，演示租户基础模块。
* 系统设置：账本设置，设置租户的基本信息。
---

### 演示

###### 平台运维
* 演示站点：http://localhost:8080/platform/login
* 演示账号/密码： admin/admin123
---
###### 租户管理
* 演示站点：http://localhost:8080/login
* 演示账号/密码： baidu/admin123

### 技术交流

技术交流，可以联系，QQ:278220723
