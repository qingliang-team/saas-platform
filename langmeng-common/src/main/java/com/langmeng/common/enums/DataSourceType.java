package com.langmeng.common.enums;

/**
 * 数据源
 * 
 * @author damaomi
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
