package com.langmeng.common.enums;

public enum LoginType {

    /** 用户登录类型 */
    SAAS_USER("saas_user", "平台用户"),
    PLATFORM_USER("platform_user", "运营用户");

    private String type;
    private String typeMsg;

    public String getType() {
        return type;
    }

    public String getTypeMsg() {
        return typeMsg;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setTypeMsg(String typeMsg) {
        this.typeMsg = typeMsg;
    }

    private LoginType(String type, String typeMsg) {
        this.type = type;
        this.typeMsg = typeMsg;
    }

}
