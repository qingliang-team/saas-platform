package com.langmeng.platform.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.langmeng.common.annotation.Excel;
import com.langmeng.common.core.domain.BaseEntity;

/**
 * 租户管理对象 platform_tenant
 * 
 * @author 大猫咪
 * @date 2022-07-29
 */
public class PlatformTenant extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 租户ID */
    private Long tenantId;

    /** 租户名称 */
    @Excel(name = "租户名称")
    private String tenantName;

    /** 租户编号 */
    @Excel(name = "租户编号")
    private String tenantNo;

    /** 租户logo */
    @Excel(name = "租户logo")
    private String logoUrl;

    /** 租户类型 */
    @Excel(name = "租户类型")
    private String tenantType;

    /** 租户状态（0正常 1停用） */
    @Excel(name = "租户状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 租户系统开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "租户系统开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startDate;

    /** 租户系统到期时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "租户系统到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endDate;

    public void setTenantId(Long tenantId) 
    {
        this.tenantId = tenantId;
    }

    public Long getTenantId() 
    {
        return tenantId;
    }
    public void setTenantName(String tenantName) 
    {
        this.tenantName = tenantName;
    }

    public String getTenantName() 
    {
        return tenantName;
    }
    public void setTenantNo(String tenantNo) 
    {
        this.tenantNo = tenantNo;
    }

    public String getTenantNo() 
    {
        return tenantNo;
    }
    public void setLogoUrl(String logoUrl) 
    {
        this.logoUrl = logoUrl;
    }

    public String getLogoUrl() 
    {
        return logoUrl;
    }
    public void setTenantType(String tenantType) 
    {
        this.tenantType = tenantType;
    }

    public String getTenantType() 
    {
        return tenantType;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setStartDate(Date startDate) 
    {
        this.startDate = startDate;
    }

    public Date getStartDate() 
    {
        return startDate;
    }
    public void setEndDate(Date endDate) 
    {
        this.endDate = endDate;
    }

    public Date getEndDate() 
    {
        return endDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("tenantId", getTenantId())
            .append("tenantName", getTenantName())
            .append("tenantNo", getTenantNo())
            .append("logoUrl", getLogoUrl())
            .append("tenantType", getTenantType())
            .append("status", getStatus())
            .append("remark", getRemark())
            .append("startDate", getStartDate())
            .append("endDate", getEndDate())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
