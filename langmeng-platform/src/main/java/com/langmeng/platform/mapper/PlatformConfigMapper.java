package com.langmeng.platform.mapper;

import com.langmeng.platform.domain.PlatformConfig;

import java.util.List;

/**
 * 参数配置 数据层
 * 
 * @author damaomi
 */
public interface PlatformConfigMapper
{
    /**
     * 查询参数配置信息
     * 
     * @param config 参数配置信息
     * @return 参数配置信息
     */
    public PlatformConfig selectConfig(PlatformConfig config);

    /**
     * 查询参数配置列表
     * 
     * @param config 参数配置信息
     * @return 参数配置集合
     */
    public List<PlatformConfig> selectConfigList(PlatformConfig config);

    /**
     * 根据键名查询参数配置信息
     * 
     * @param configKey 参数键名
     * @return 参数配置信息
     */
    public PlatformConfig checkConfigKeyUnique(String configKey);

    /**
     * 新增参数配置
     * 
     * @param config 参数配置信息
     * @return 结果
     */
    public int insertConfig(PlatformConfig config);

    /**
     * 修改参数配置
     * 
     * @param config 参数配置信息
     * @return 结果
     */
    public int updateConfig(PlatformConfig config);

    /**
     * 删除参数配置
     * 
     * @param configId 参数主键
     * @return 结果
     */
    public int deleteConfigById(Long configId);

    /**
     * 批量删除参数配置
     * 
     * @param configIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteConfigByIds(String[] configIds);
}