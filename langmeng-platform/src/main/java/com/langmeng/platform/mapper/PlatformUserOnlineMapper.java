package com.langmeng.platform.mapper;

import com.langmeng.platform.domain.PlatformUserOnline;

import java.util.List;

/**
 * 在线用户 数据层
 * 
 * @author damaomi
 */
public interface PlatformUserOnlineMapper
{
    /**
     * 通过会话序号查询信息
     * 
     * @param sessionId 会话ID
     * @return 在线用户信息
     */
    public PlatformUserOnline selectOnlineById(String sessionId);

    /**
     * 通过会话序号删除信息
     * 
     * @param sessionId 会话ID
     * @return 在线用户信息
     */
    public int deleteOnlineById(String sessionId);

    /**
     * 保存会话信息
     * 
     * @param online 会话信息
     * @return 结果
     */
    public int saveOnline(PlatformUserOnline online);

    /**
     * 查询会话集合
     * 
     * @param userOnline 会话参数
     * @return 会话集合
     */
    public List<PlatformUserOnline> selectUserOnlineList(PlatformUserOnline userOnline);

    /**
     * 查询过期会话集合
     * 
     * @param lastAccessTime 过期时间
     * @return 会话集合
     */
    public List<PlatformUserOnline> selectOnlineByExpired(String lastAccessTime);
}
