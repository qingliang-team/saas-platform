package com.langmeng.platform.service.impl;

import java.util.List;
import com.langmeng.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.langmeng.platform.mapper.PlatformTenantMapper;
import com.langmeng.platform.domain.PlatformTenant;
import com.langmeng.platform.service.IPlatformTenantService;
import com.langmeng.common.core.text.Convert;

/**
 * 租户管理Service业务层处理
 * 
 * @author 大猫咪
 * @date 2022-07-29
 */
@Service
public class PlatformTenantServiceImpl implements IPlatformTenantService 
{
    @Autowired
    private PlatformTenantMapper platformTenantMapper;

    /**
     * 查询租户管理
     * 
     * @param tenantId 租户管理主键
     * @return 租户管理
     */
    @Override
    public PlatformTenant selectPlatformTenantByTenantId(Long tenantId)
    {
        return platformTenantMapper.selectPlatformTenantByTenantId(tenantId);
    }

    /**
     * 查询租户管理列表
     * 
     * @param platformTenant 租户管理
     * @return 租户管理
     */
    @Override
    public List<PlatformTenant> selectPlatformTenantList(PlatformTenant platformTenant)
    {
        return platformTenantMapper.selectPlatformTenantList(platformTenant);
    }

    /**
     * 新增租户管理
     * 
     * @param platformTenant 租户管理
     * @return 结果
     */
    @Override
    public int insertPlatformTenant(PlatformTenant platformTenant)
    {
        platformTenant.setCreateTime(DateUtils.getNowDate());
        return platformTenantMapper.insertPlatformTenant(platformTenant);
    }

    /**
     * 修改租户管理
     * 
     * @param platformTenant 租户管理
     * @return 结果
     */
    @Override
    public int updatePlatformTenant(PlatformTenant platformTenant)
    {
        platformTenant.setUpdateTime(DateUtils.getNowDate());
        return platformTenantMapper.updatePlatformTenant(platformTenant);
    }

    /**
     * 批量删除租户管理
     * 
     * @param tenantIds 需要删除的租户管理主键
     * @return 结果
     */
    @Override
    public int deletePlatformTenantByTenantIds(String tenantIds)
    {
        return platformTenantMapper.deletePlatformTenantByTenantIds(Convert.toStrArray(tenantIds));
    }

    /**
     * 删除租户管理信息
     * 
     * @param tenantId 租户管理主键
     * @return 结果
     */
    @Override
    public int deletePlatformTenantByTenantId(Long tenantId)
    {
        return platformTenantMapper.deletePlatformTenantByTenantId(tenantId);
    }
}
