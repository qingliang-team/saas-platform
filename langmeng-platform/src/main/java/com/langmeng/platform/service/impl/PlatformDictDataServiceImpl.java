package com.langmeng.platform.service.impl;

import com.langmeng.common.core.domain.platform.PlatformDictData;
import com.langmeng.common.core.text.Convert;
import com.langmeng.common.utils.PlatformDictUtils;
import com.langmeng.platform.mapper.PlatformDictDataMapper;
import com.langmeng.platform.service.IPlatformDictDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 字典 业务层处理
 * 
 * @author damaomi
 */
@Service
public class PlatformDictDataServiceImpl implements IPlatformDictDataService
{
    @Autowired
    private PlatformDictDataMapper dictDataMapper;

    /**
     * 根据条件分页查询字典数据
     * 
     * @param dictData 字典数据信息
     * @return 字典数据集合信息
     */
    @Override
    public List<PlatformDictData> selectDictDataList(PlatformDictData dictData)
    {
        return dictDataMapper.selectDictDataList(dictData);
    }

    /**
     * 根据字典类型和字典键值查询字典数据信息
     * 
     * @param dictType 字典类型
     * @param dictValue 字典键值
     * @return 字典标签
     */
    @Override
    public String selectDictLabel(String dictType, String dictValue)
    {
        return dictDataMapper.selectPlatformDictLabel(dictType, dictValue);
    }

    /**
     * 根据字典数据ID查询信息
     * 
     * @param dictCode 字典数据ID
     * @return 字典数据
     */
    @Override
    public PlatformDictData selectDictDataById(Long dictCode)
    {
        return dictDataMapper.selectDictDataById(dictCode);
    }

    /**
     * 批量删除字典数据
     * 
     * @param ids 需要删除的数据
     */
    @Override
    public void deleteDictDataByIds(String ids)
    {
        Long[] dictCodes = Convert.toLongArray(ids);
        for (Long dictCode : dictCodes)
        {
            PlatformDictData data = selectDictDataById(dictCode);
            dictDataMapper.deleteDictDataById(dictCode);
            List<PlatformDictData> dictDatas = dictDataMapper.selectDictDataByType(data.getDictType());
            PlatformDictUtils.setDictCache(data.getDictType(), dictDatas);
        }
    }

    /**
     * 新增保存字典数据信息
     * 
     * @param data 字典数据信息
     * @return 结果
     */
    @Override
    public int insertDictData(PlatformDictData data)
    {
        int row = dictDataMapper.insertDictData(data);
        if (row > 0)
        {
            List<PlatformDictData> dictDatas = dictDataMapper.selectDictDataByType(data.getDictType());
            PlatformDictUtils.setDictCache(data.getDictType(), dictDatas);
        }
        return row;
    }

    /**
     * 修改保存字典数据信息
     * 
     * @param data 字典数据信息
     * @return 结果
     */
    @Override
    public int updateDictData(PlatformDictData data)
    {
        int row = dictDataMapper.updateDictData(data);
        if (row > 0)
        {
            List<PlatformDictData> dictDatas = dictDataMapper.selectDictDataByType(data.getDictType());
            PlatformDictUtils.setDictCache(data.getDictType(), dictDatas);
        }
        return row;
    }
}
