package com.langmeng.platform.service;

import java.util.List;
import com.langmeng.platform.domain.PlatformTenant;

/**
 * 租户管理Service接口
 * 
 * @author 大猫咪
 * @date 2022-07-29
 */
public interface IPlatformTenantService 
{
    /**
     * 查询租户管理
     * 
     * @param tenantId 租户管理主键
     * @return 租户管理
     */
    public PlatformTenant selectPlatformTenantByTenantId(Long tenantId);

    /**
     * 查询租户管理列表
     * 
     * @param platformTenant 租户管理
     * @return 租户管理集合
     */
    public List<PlatformTenant> selectPlatformTenantList(PlatformTenant platformTenant);

    /**
     * 新增租户管理
     * 
     * @param platformTenant 租户管理
     * @return 结果
     */
    public int insertPlatformTenant(PlatformTenant platformTenant);

    /**
     * 修改租户管理
     * 
     * @param platformTenant 租户管理
     * @return 结果
     */
    public int updatePlatformTenant(PlatformTenant platformTenant);

    /**
     * 批量删除租户管理
     * 
     * @param tenantIds 需要删除的租户管理主键集合
     * @return 结果
     */
    public int deletePlatformTenantByTenantIds(String tenantIds);

    /**
     * 删除租户管理信息
     * 
     * @param tenantId 租户管理主键
     * @return 结果
     */
    public int deletePlatformTenantByTenantId(Long tenantId);
}
