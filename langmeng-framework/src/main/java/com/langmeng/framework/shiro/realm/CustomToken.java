package com.langmeng.framework.shiro.realm;

import org.apache.shiro.authc.UsernamePasswordToken;

public class CustomToken extends UsernamePasswordToken {

    // 定义登录的类型是为了后面的校验中，选择使用哪一个realm
    private  String loginType;

    // 租户编号
    private String tenantNo;

    public CustomToken(String userName,String password,boolean rememberMe,String loginType) {
        super(userName, password, rememberMe);
        this.loginType = loginType;
    }

    public CustomToken(String userName,String password,boolean rememberMe,String tenantNo,String loginType) {
        super(userName, password, rememberMe);
        this.tenantNo = tenantNo;
        this.loginType = loginType;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getTenantNo() {
        return tenantNo;
    }

    public void setTenantNo(String tenantNo) {
        this.tenantNo = tenantNo;
    }
}
