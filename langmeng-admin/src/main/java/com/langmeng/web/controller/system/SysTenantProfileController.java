package com.langmeng.web.controller.system;

import com.langmeng.common.annotation.Log;
import com.langmeng.common.config.LangMengConfig;
import com.langmeng.common.core.controller.BaseController;
import com.langmeng.common.core.domain.AjaxResult;
import com.langmeng.common.core.domain.saas.SysUser;
import com.langmeng.common.enums.BusinessType;
import com.langmeng.common.utils.file.FileUploadUtils;
import com.langmeng.common.utils.file.MimeTypeUtils;
import com.langmeng.platform.domain.PlatformTenant;
import com.langmeng.platform.service.IPlatformTenantService;
import com.langmeng.system.service.ISysUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * 账户信息 业务处理
 * 
 * @author damaomi
 */
@Controller
@RequestMapping("/system/tenant/profile")
public class SysTenantProfileController extends BaseController
{
    private static final Logger log = LoggerFactory.getLogger(SysTenantProfileController.class);

    private String prefix = "system/tenant/profile";

    @Autowired
    private ISysUserService userService;

    @Autowired
    private IPlatformTenantService platformTenantService;
    
    /**
     * 账本信息
     */
    @GetMapping()
    public String profile(ModelMap mmap)
    {
        SysUser user = getSysUser();
        // 获取账当前户实体
        PlatformTenant platformTenant = platformTenantService.selectPlatformTenantByTenantId(user.getTenantId());
        mmap.put("platformTenant", platformTenant);
        return prefix + "/profile";
    }

    /**
     * 修改账本
     */
    @GetMapping("/edit")
    public String edit(ModelMap mmap)
    {
        SysUser user = getSysUser();
        mmap.put("user", userService.selectUserById(user.getUserId()));
        return prefix + "/edit";
    }

    /**
     * 修改头像
     */
    @GetMapping("/avatar")
    public String avatar(ModelMap mmap)
    {
        SysUser user = getSysUser();
        mmap.put("user", userService.selectUserById(user.getUserId()));
        return prefix + "/avatar";
    }

    /**
     * 修改用户
     */
    @Log(title = "账户信息", businessType = BusinessType.UPDATE)
    @PostMapping("/update")
    @ResponseBody
    public AjaxResult update(PlatformTenant tenant) {
        SysUser user = getSysUser();
        // 获取账当前户实体
        PlatformTenant currentTenant = platformTenantService.selectPlatformTenantByTenantId(user.getTenantId());
        currentTenant.setTenantName(tenant.getTenantName());
        currentTenant.setTenantNo(tenant.getTenantNo());
        currentTenant.setLogoUrl(tenant.getLogoUrl());
        currentTenant.setRemark(tenant.getRemark());
        // 更新账户信息实体
        if (platformTenantService.updatePlatformTenant(currentTenant) > 0) {
            return success();
        }
        return error();
    }

    /**
     * 保存头像
     */
    @Log(title = "个人信息", businessType = BusinessType.UPDATE)
    @PostMapping("/updateAvatar")
    @ResponseBody
    public AjaxResult updateAvatar(@RequestParam("avatarfile") MultipartFile file)
    {
        SysUser currentUser = getSysUser();
        try
        {
            if (!file.isEmpty())
            {
                String avatar = FileUploadUtils.upload(LangMengConfig.getAvatarPath(), file, MimeTypeUtils.IMAGE_EXTENSION);
                currentUser.setAvatar(avatar);
                if (userService.updateUserInfo(currentUser) > 0)
                {
                    setSysUser(userService.selectUserById(currentUser.getUserId()));
                    return success();
                }
            }
            return error();
        }
        catch (Exception e)
        {
            log.error("修改头像失败！", e);
            return error(e.getMessage());
        }
    }
}
