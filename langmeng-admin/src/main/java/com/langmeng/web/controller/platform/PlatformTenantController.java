package com.langmeng.web.controller.platform;

import java.util.List;

import com.langmeng.platform.domain.PlatformTenant;
import com.langmeng.platform.service.IPlatformTenantService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.langmeng.common.annotation.Log;
import com.langmeng.common.enums.BusinessType;
import com.langmeng.common.core.controller.BaseController;
import com.langmeng.common.core.domain.AjaxResult;
import com.langmeng.common.utils.poi.ExcelUtil;
import com.langmeng.common.core.page.TableDataInfo;

/**
 * 租户管理Controller
 * 
 * @author 大猫咪
 * @date 2022-07-29
 */
@Controller
@RequestMapping("/platform/tenant")
public class PlatformTenantController extends BaseController
{
    private String prefix = "platform/tenant";

    @Autowired
    private IPlatformTenantService platformTenantService;

    @RequiresPermissions("platform:tenant:view")
    @GetMapping()
    public String tenant()
    {
        return prefix + "/tenant";
    }

    /**
     * 查询租户管理列表
     */
    @RequiresPermissions("platform:tenant:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PlatformTenant platformTenant)
    {
        startPage();
        List<PlatformTenant> list = platformTenantService.selectPlatformTenantList(platformTenant);
        return getDataTable(list);
    }

    /**
     * 导出租户管理列表
     */
    @RequiresPermissions("platform:tenant:export")
    @Log(title = "租户管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PlatformTenant platformTenant)
    {
        List<PlatformTenant> list = platformTenantService.selectPlatformTenantList(platformTenant);
        ExcelUtil<PlatformTenant> util = new ExcelUtil<PlatformTenant>(PlatformTenant.class);
        return util.exportExcel(list, "租户管理数据");
    }

    /**
     * 新增租户管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存租户管理
     */
    @RequiresPermissions("platform:tenant:add")
    @Log(title = "租户管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PlatformTenant platformTenant)
    {
        return toAjax(platformTenantService.insertPlatformTenant(platformTenant));
    }

    /**
     * 修改租户管理
     */
    @RequiresPermissions("platform:tenant:edit")
    @GetMapping("/edit/{tenantId}")
    public String edit(@PathVariable("tenantId") Long tenantId, ModelMap mmap)
    {
        PlatformTenant platformTenant = platformTenantService.selectPlatformTenantByTenantId(tenantId);
        mmap.put("platformTenant", platformTenant);
        return prefix + "/edit";
    }

    /**
     * 修改保存租户管理
     */
    @RequiresPermissions("platform:tenant:edit")
    @Log(title = "租户管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PlatformTenant platformTenant)
    {
        return toAjax(platformTenantService.updatePlatformTenant(platformTenant));
    }

    /**
     * 删除租户管理
     */
    @RequiresPermissions("platform:tenant:remove")
    @Log(title = "租户管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(platformTenantService.deletePlatformTenantByTenantIds(ids));
    }
}
