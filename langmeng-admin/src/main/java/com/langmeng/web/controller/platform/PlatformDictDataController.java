package com.langmeng.web.controller.platform;

import com.langmeng.common.annotation.Log;
import com.langmeng.common.core.controller.BaseController;
import com.langmeng.common.core.domain.AjaxResult;
import com.langmeng.common.core.domain.platform.PlatformDictData;
import com.langmeng.common.core.page.TableDataInfo;
import com.langmeng.common.enums.BusinessType;
import com.langmeng.common.utils.poi.ExcelUtil;
import com.langmeng.platform.service.IPlatformDictDataService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 数据字典信息
 * 
 * @author damaomi
 */
@Controller
@RequestMapping("/platform/dict/data")
public class PlatformDictDataController extends BaseController
{
    private String prefix = "platform/dict/data";

    @Autowired
    private IPlatformDictDataService dictDataService;

    @RequiresPermissions("platform:dict:view")
    @GetMapping()
    public String dictData()
    {
        return prefix + "/data";
    }

    @PostMapping("/list")
    @RequiresPermissions("platform:dict:list")
    @ResponseBody
    public TableDataInfo list(PlatformDictData dictData)
    {
        startPage();
        List<PlatformDictData> list = dictDataService.selectDictDataList(dictData);
        return getDataTable(list);
    }

    @Log(title = "字典数据", businessType = BusinessType.EXPORT)
    @RequiresPermissions("platform:dict:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PlatformDictData dictData)
    {
        List<PlatformDictData> list = dictDataService.selectDictDataList(dictData);
        ExcelUtil<PlatformDictData> util = new ExcelUtil<PlatformDictData>(PlatformDictData.class);
        return util.exportExcel(list, "字典数据");
    }

    /**
     * 新增字典类型
     */
    @GetMapping("/add/{dictType}")
    public String add(@PathVariable("dictType") String dictType, ModelMap mmap)
    {
        mmap.put("dictType", dictType);
        return prefix + "/add";
    }

    /**
     * 新增保存字典类型
     */
    @Log(title = "字典数据", businessType = BusinessType.INSERT)
    @RequiresPermissions("platform:dict:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated PlatformDictData dict)
    {
        dict.setCreateBy(getLoginName());
        return toAjax(dictDataService.insertDictData(dict));
    }

    /**
     * 修改字典类型
     */
    @RequiresPermissions("platform:dict:edit")
    @GetMapping("/edit/{dictCode}")
    public String edit(@PathVariable("dictCode") Long dictCode, ModelMap mmap)
    {
        mmap.put("dict", dictDataService.selectDictDataById(dictCode));
        return prefix + "/edit";
    }

    /**
     * 修改保存字典类型
     */
    @Log(title = "字典数据", businessType = BusinessType.UPDATE)
    @RequiresPermissions("platform:dict:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@Validated PlatformDictData dict)
    {
        dict.setUpdateBy(getLoginName());
        return toAjax(dictDataService.updateDictData(dict));
    }

    @Log(title = "字典数据", businessType = BusinessType.DELETE)
    @RequiresPermissions("platform:dict:remove")
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        dictDataService.deleteDictDataByIds(ids);
        return success();
    }
}
