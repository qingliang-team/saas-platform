package com.langmeng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 * 
 * @author damaomi
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class LangmengApplication
{
    public static void main(String[] args)
    {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(LangmengApplication.class, args);
        System.out.println("========== SaaS-Platform 系统启动成功========== \n");
    }
}