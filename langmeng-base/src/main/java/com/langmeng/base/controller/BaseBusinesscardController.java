package com.langmeng.base.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.langmeng.common.annotation.Log;
import com.langmeng.common.enums.BusinessType;
import com.langmeng.base.domain.BaseBusinesscard;
import com.langmeng.base.service.IBaseBusinesscardService;
import com.langmeng.common.core.controller.BaseController;
import com.langmeng.common.core.domain.AjaxResult;
import com.langmeng.common.utils.poi.ExcelUtil;
import com.langmeng.common.core.page.TableDataInfo;

/**
 * 名片管理Controller
 * 
 * @author 大猫咪
 * @date 2022-08-02
 */
@Controller
@RequestMapping("/base/businesscard")
public class BaseBusinesscardController extends BaseController
{
    private String prefix = "base/businesscard";

    @Autowired
    private IBaseBusinesscardService baseBusinesscardService;

    @RequiresPermissions("base:businesscard:view")
    @GetMapping()
    public String businesscard()
    {
        return prefix + "/businesscard";
    }

    /**
     * 查询名片管理列表
     */
    @RequiresPermissions("base:businesscard:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BaseBusinesscard baseBusinesscard)
    {
        startPage();
        List<BaseBusinesscard> list = baseBusinesscardService.selectBaseBusinesscardList(baseBusinesscard);
        return getDataTable(list);
    }

    /**
     * 导出名片管理列表
     */
    @RequiresPermissions("base:businesscard:export")
    @Log(title = "名片管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BaseBusinesscard baseBusinesscard)
    {
        List<BaseBusinesscard> list = baseBusinesscardService.selectBaseBusinesscardList(baseBusinesscard);
        ExcelUtil<BaseBusinesscard> util = new ExcelUtil<BaseBusinesscard>(BaseBusinesscard.class);
        return util.exportExcel(list, "名片管理数据");
    }

    /**
     * 新增名片管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存名片管理
     */
    @RequiresPermissions("base:businesscard:add")
    @Log(title = "名片管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BaseBusinesscard baseBusinesscard)
    {
        return toAjax(baseBusinesscardService.insertBaseBusinesscard(baseBusinesscard));
    }

    /**
     * 修改名片管理
     */
    @RequiresPermissions("base:businesscard:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        BaseBusinesscard baseBusinesscard = baseBusinesscardService.selectBaseBusinesscardById(id);
        mmap.put("baseBusinesscard", baseBusinesscard);
        return prefix + "/edit";
    }

    /**
     * 修改保存名片管理
     */
    @RequiresPermissions("base:businesscard:edit")
    @Log(title = "名片管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BaseBusinesscard baseBusinesscard)
    {
        return toAjax(baseBusinesscardService.updateBaseBusinesscard(baseBusinesscard));
    }

    /**
     * 删除名片管理
     */
    @RequiresPermissions("base:businesscard:remove")
    @Log(title = "名片管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(baseBusinesscardService.deleteBaseBusinesscardByIds(ids));
    }
}
