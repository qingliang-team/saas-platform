package com.langmeng.base.service.impl;

import java.util.List;

import com.langmeng.common.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.langmeng.base.mapper.BaseBusinesscardMapper;
import com.langmeng.base.domain.BaseBusinesscard;
import com.langmeng.base.service.IBaseBusinesscardService;
import com.langmeng.common.core.text.Convert;

/**
 * 名片管理Service业务层处理
 * 
 * @author 大猫咪
 * @date 2022-08-02
 */
@Service
public class BaseBusinesscardServiceImpl implements IBaseBusinesscardService 
{
    @Autowired
    private BaseBusinesscardMapper baseBusinesscardMapper;

    /**
     * 查询名片管理
     * 
     * @param id 名片管理主键
     * @return 名片管理
     */
    @Override
    public BaseBusinesscard selectBaseBusinesscardById(Long id)
    {
        return baseBusinesscardMapper.selectBaseBusinesscardById(id);
    }

    /**
     * 查询名片管理列表
     * 
     * @param baseBusinesscard 名片管理
     * @return 名片管理
     */
    @Override
    public List<BaseBusinesscard> selectBaseBusinesscardList(BaseBusinesscard baseBusinesscard)
    {
        // 获取租户编号
        Long tenantId = ShiroUtils.getSysUser().getTenantId();
        baseBusinesscard.setTenantId(tenantId);
        return baseBusinesscardMapper.selectBaseBusinesscardList(baseBusinesscard);
    }

    /**
     * 新增名片管理
     * 
     * @param baseBusinesscard 名片管理
     * @return 结果
     */
    @Override
    public int insertBaseBusinesscard(BaseBusinesscard baseBusinesscard)
    {
        // 获取租户编号
        Long tenantId = ShiroUtils.getSysUser().getTenantId();
        baseBusinesscard.setTenantId(tenantId);
        return baseBusinesscardMapper.insertBaseBusinesscard(baseBusinesscard);
    }

    /**
     * 修改名片管理
     * 
     * @param baseBusinesscard 名片管理
     * @return 结果
     */
    @Override
    public int updateBaseBusinesscard(BaseBusinesscard baseBusinesscard)
    {
        return baseBusinesscardMapper.updateBaseBusinesscard(baseBusinesscard);
    }

    /**
     * 批量删除名片管理
     * 
     * @param ids 需要删除的名片管理主键
     * @return 结果
     */
    @Override
    public int deleteBaseBusinesscardByIds(String ids)
    {
        return baseBusinesscardMapper.deleteBaseBusinesscardByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除名片管理信息
     * 
     * @param id 名片管理主键
     * @return 结果
     */
    @Override
    public int deleteBaseBusinesscardById(Long id)
    {
        return baseBusinesscardMapper.deleteBaseBusinesscardById(id);
    }
}
