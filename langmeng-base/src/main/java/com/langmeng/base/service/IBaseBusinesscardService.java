package com.langmeng.base.service;

import java.util.List;
import com.langmeng.base.domain.BaseBusinesscard;

/**
 * 名片管理Service接口
 * 
 * @author 大猫咪
 * @date 2022-08-02
 */
public interface IBaseBusinesscardService 
{
    /**
     * 查询名片管理
     * 
     * @param id 名片管理主键
     * @return 名片管理
     */
    public BaseBusinesscard selectBaseBusinesscardById(Long id);

    /**
     * 查询名片管理列表
     * 
     * @param baseBusinesscard 名片管理
     * @return 名片管理集合
     */
    public List<BaseBusinesscard> selectBaseBusinesscardList(BaseBusinesscard baseBusinesscard);

    /**
     * 新增名片管理
     * 
     * @param baseBusinesscard 名片管理
     * @return 结果
     */
    public int insertBaseBusinesscard(BaseBusinesscard baseBusinesscard);

    /**
     * 修改名片管理
     * 
     * @param baseBusinesscard 名片管理
     * @return 结果
     */
    public int updateBaseBusinesscard(BaseBusinesscard baseBusinesscard);

    /**
     * 批量删除名片管理
     * 
     * @param ids 需要删除的名片管理主键集合
     * @return 结果
     */
    public int deleteBaseBusinesscardByIds(String ids);

    /**
     * 删除名片管理信息
     * 
     * @param id 名片管理主键
     * @return 结果
     */
    public int deleteBaseBusinesscardById(Long id);
}
