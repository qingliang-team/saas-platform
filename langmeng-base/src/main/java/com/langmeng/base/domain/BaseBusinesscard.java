package com.langmeng.base.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.langmeng.common.annotation.Excel;
import com.langmeng.common.core.domain.BaseEntity;

/**
 * 名片管理对象 base_businesscard
 * 
 * @author 大猫咪
 * @date 2022-08-02
 */
public class BaseBusinesscard extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 名片ID */
    private Long id;

    /** 姓名 */
    @Excel(name = "姓名")
    private String fullName;

    /** 公司 */
    @Excel(name = "公司")
    private String company;

    /** 职务 */
    @Excel(name = "职务")
    private String title;

    /** 电话 */
    @Excel(name = "电话")
    private String phone;

    /** 邮编 */
    @Excel(name = "邮编")
    private String postalCode;

    /** 手机 */
    @Excel(name = "手机")
    private String mobile;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 邮箱 */
    @Excel(name = "邮箱")
    private String email;

    /** 办公电话 */
    @Excel(name = "办公电话")
    private String officePhone;

    /** QQ */
    @Excel(name = "QQ")
    private String qq;

    /** 传真 */
    @Excel(name = "传真")
    private String fax;

    /** 主页 */
    @Excel(name = "主页")
    private String web;

    /** 银行 */
    private String bankName;

    /** 账户 */
    private String bankAccount;

    /** 税号 */
    private String taxAccount;

    /** 是否个人 */
    private Long personal;

    /** 删除标志 */
    private Long deletionstateCode;

    /** 显示顺序 */
    private Long sortCode;

    /** 租户ID */
    private Long tenantId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setFullName(String fullName) 
    {
        this.fullName = fullName;
    }

    public String getFullName() 
    {
        return fullName;
    }
    public void setCompany(String company) 
    {
        this.company = company;
    }

    public String getCompany() 
    {
        return company;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setPostalCode(String postalCode) 
    {
        this.postalCode = postalCode;
    }

    public String getPostalCode() 
    {
        return postalCode;
    }
    public void setMobile(String mobile) 
    {
        this.mobile = mobile;
    }

    public String getMobile() 
    {
        return mobile;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setOfficePhone(String officePhone) 
    {
        this.officePhone = officePhone;
    }

    public String getOfficePhone() 
    {
        return officePhone;
    }
    public void setQq(String qq) 
    {
        this.qq = qq;
    }

    public String getQq() 
    {
        return qq;
    }
    public void setFax(String fax) 
    {
        this.fax = fax;
    }

    public String getFax() 
    {
        return fax;
    }
    public void setWeb(String web) 
    {
        this.web = web;
    }

    public String getWeb() 
    {
        return web;
    }
    public void setBankName(String bankName) 
    {
        this.bankName = bankName;
    }

    public String getBankName() 
    {
        return bankName;
    }
    public void setBankAccount(String bankAccount) 
    {
        this.bankAccount = bankAccount;
    }

    public String getBankAccount() 
    {
        return bankAccount;
    }
    public void setTaxAccount(String taxAccount) 
    {
        this.taxAccount = taxAccount;
    }

    public String getTaxAccount() 
    {
        return taxAccount;
    }
    public void setPersonal(Long personal) 
    {
        this.personal = personal;
    }

    public Long getPersonal() 
    {
        return personal;
    }
    public void setDeletionstateCode(Long deletionstateCode) 
    {
        this.deletionstateCode = deletionstateCode;
    }

    public Long getDeletionstateCode() 
    {
        return deletionstateCode;
    }
    public void setSortCode(Long sortCode) 
    {
        this.sortCode = sortCode;
    }

    public Long getSortCode() 
    {
        return sortCode;
    }
    public void setTenantId(Long tenantId) 
    {
        this.tenantId = tenantId;
    }

    public Long getTenantId() 
    {
        return tenantId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("fullName", getFullName())
            .append("company", getCompany())
            .append("title", getTitle())
            .append("phone", getPhone())
            .append("postalCode", getPostalCode())
            .append("mobile", getMobile())
            .append("address", getAddress())
            .append("email", getEmail())
            .append("officePhone", getOfficePhone())
            .append("qq", getQq())
            .append("fax", getFax())
            .append("web", getWeb())
            .append("bankName", getBankName())
            .append("bankAccount", getBankAccount())
            .append("taxAccount", getTaxAccount())
            .append("personal", getPersonal())
            .append("deletionstateCode", getDeletionstateCode())
            .append("sortCode", getSortCode())
            .append("remark", getRemark())
            .append("tenantId", getTenantId())
            .toString();
    }
}
